#!/bin/bash

echo "extract: ver.1.1: Extract audio tracks from multimedia (video) container"

usage()
{
	echo "Usage:"
	echo "extract.sh <video_container> -t track_number -c audio_codec -b bitrate -d dynamic_range -m downmix -s suffix"
	echo "Where:"
	echo "  video_container - The video-file containing the desired audio-track"
	echo "     track_number - The number of desired audio-track: 1 or 2 or 1,2 or a (-t --track)"
	echo "                    For multiple track extraction specify for example: 1,2,5,7,16"
	echo "                    For extraction of all audio tracks specify: a or A"
	echo "      audio_codec - The name of audio codec: ffmpeg -codecs (-c --codec)"
	echo "          bitrate - The desired bitrate: 320k for MP3 or 529k for AAC (-b --bitrate)"
	echo "    dynamic_range - The desired dynamic-range: 16 or 32 bits (-d --dynrange)"
	echo "          downmix - The number of channels for down-mixing the multichannel track to stereo: 6 or 8 (-m --multi)"
	echo "           suffix - The suffix for output file: Rus,Ukr,Eng (-s --suffix)"
}

VOLUME_DETECT=volumedetect
PAN_6_TO_STEREO='pan=stereo|FL=0.5*FC+0.707*FL+0.707*BL+0.5*LFE|FR=0.5*FC+0.707*FR+0.707*BR+0.5*LFE'
PAN_8_TO_STEREO='pan=stereo|FL=0.5*FC+0.707*FL+0.5*SL+0.707*BL+0.5*LFE|FR=0.5*FC+0.707*FR+0.5*SR+0.707*BR+0.5*LFE'

# -----------------------------------------------------------------------------
# $1 - input file
# $2 - flags
# $3 - output file
ffmpeg_extract_audio()
{
	rm "${3}"
	echo "ffmpeg -i \"${1}\" ${2} \"${3}\""
	ffmpeg -i "${1}" ${2} "${3}"
}

# -----------------------------------------------------------------------------
extract_audio()
{
	if [[ -z $MULTI ]]; then
		FILTER=
	elif [[ $MULTI == "6" ]]; then
		FILTER="-af ${PAN_6_TO_STEREO}"
	elif [[ $MULTI == "8" ]]; then
		FILTER="-af ${PAN_8_TO_STEREO}"
	fi

	if [[ ! -z $CODEC ]]; then
		ACODEC="-c:a ${CODEC}"
		if [[ $CODEC == "flac" ]]; then
			EXT=flac
		elif [[ $CODEC == "aac" ]]; then
			EXT=m4a
		else
			EXT=raw
		fi
	else
		ACODEC="-c:a copy"
	fi

	if [[ ! -z $BITRATE ]]; then
		ABITRATE="-b:a ${BITRATE}"
	fi 

	if [[ ! -z $DYNRANGE ]]; then
		ADYNRANGE="-sample_fmt s${DYNRANGE}"
	fi 

	# Differentiate the path of the source file
	SRC_FILE=$(basename -- "${CONTAINER}")
	SRC_FILE_EXT="${SRC_FILE##*.}"
	SRC_FILE_NAME="${SRC_FILE%.*}"

	# In case all audio-tracks are reqiured by "-t a" arguments
	# Extract all audio-tracks to output .mkv container
	if [[ ${TRACK} -eq "a" || ${TRACK} -eq "A" ]]; then
		local OUT_FILE=${SRC_FILE_NAME}-audio-tracks.mkv
		local FFMPEG_FLAGS="-v quiet -stats -map 0:a ${ACODEC} ${ABITRATE} ${ADYNRANGE} ${FILTER}"
		echo "==> Extracting ALL audio-tracks from: ${CONTAINER} to: ==> ${OUT_FILE}"
		ffmpeg_extract_audio "${CONTAINER}" "${FFMPEG_FLAGS}" "${OUT_FILE}"
		# echo "ffmpeg -i \"${CONTAINER}\" -v quiet -stats -map 0:a ${ACODEC} ${ABITRATE} ${ADYNRANGE} ${FILTER} \"${OUT_FILE}\""
		# ffmpeg -i "${CONTAINER}" -v quiet -stats -map 0:a ${ACODEC} ${ABITRATE} ${ADYNRANGE} ${FILTER} "${OUT_FILE}"
		# Copy all global metadata: -map_metadata 0
		# Copy video-stream metadata: -map_metadata:s:v 0:s:v
		# Copy audio-stream metadata: -map_metadata:s:a 0:s:a
		return 0
	fi

	# Save number of tracks as array
	IFS=',' read -ra TRACK_ARRAY <<< "$TRACK"
	# Save suffixes as array
	read -ra SUFFIX_ARRAY <<< "$SUFFIX"

	echo "==> Extracting audio-tracks from: ${CONTAINER}"

	# Extract all desired tracks
	for track_i in "${TRACK_ARRAY[@]}"
	do
		# Check if required number of track is two or more digits
		if [[ ${#track_i} -gt 1 ]]; then
			local OUT_FILE=${SRC_FILE_NAME}.${track_i}.${EXT}
		else
			local OUT_FILE=${SRC_FILE_NAME}.0${track_i}.${EXT}
		fi
		local FFMPEG_FLAGS="-v quiet -stats -map 0:${track_i} ${ACODEC} ${ABITRATE} ${ADYNRANGE} ${FILTER}"
		echo "==> Extracting track #: ${track_i} to: ==> ${OUT_FILE}"
		ffmpeg_extract_audio "${CONTAINER}" "${FFMPEG_FLAGS}" "${OUT_FILE}"
		# echo "ffmpeg -i \"${CONTAINER}\" -v quiet -stats -map 0:${track_i} ${ACODEC} ${ABITRATE} ${ADYNRANGE} ${FILTER} \"${OUT_FILE}\""
		# ffmpeg -i "${CONTAINER}" -v quiet -stats -map 0:${track_i} ${ACODEC} ${ABITRATE} ${ADYNRANGE} ${FILTER} "${OUT_FILE}"
	done
}

# -***************************************************************************-
if [[ $# -lt 2 ]]; then
	usage
	exit 1
fi

# -***************************************************************************-
while [ "$1" != "" ]; do
	case $1 in
		-t | --track )
			if [ "$2" != "" ]; then
				TRACK=$2
				shift
			else
				usage
				exit 2
			fi
			;;
		-c | --codec )
			if [ "$2" != "" ]; then
				CODEC=$2
				shift
			else
				usage
				exit 2
			fi    
			;;
		-b | --bitrate )
			if [ "$2" != "" ]; then
				BITRATE=$2
				shift
			else
				usage
				exit 2
			fi    
			;;
		-d | --dynrange )
			if [ "$2" != "" ]; then
				DYNRANGE=$2
				shift
			else
				usage
				exit 2
			fi    
			;;
		-m | --multi )
			if [ "$2" != "" ]; then
				MULTI=$2
				shift
			else
				usage
				exit 2
			fi    
			;;
		-s | --suffix )
			if [ "$2" != "" ]; then
				SUFFIX=$2
				shift
			else
				usage
				exit 2
			fi    
			;;
		-h | --help )           
			usage
			exit
			;;
		* )                     
			CONTAINER=$1
			;;
	esac
	shift
done

extract_audio

