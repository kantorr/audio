#!/bin/bash

echo "lpf: ver.1.0: Implement Low Pass Filter to files in FLAC format with desired sample rate"

# -----------------------------------------------------------------------------
usage()
{
	echo "Usage:"
	echo "lpf.sh <folder> -f freg -t range"
	echo "Where:"
	echo " folder - The desired folder contains files to be converted"
	echo "   freq - The basic frequency for the low pass filter"
	echo "  range - The range of frequencies for the low pass filter"
	echo " Converted files will be placed in LPF-xx folder inside the required one"
}

# -----------------------------------------------------------------------------
lpf_file()
{
	echo "==> Low-pass Filtering at frequency ${FREQ} in range of ${TBW} x 2"
	sox -S "$1" --compression 5 "${OUT_DIR}/$(basename "$1" .flac).flac" sinc -${FREQ} -t ${TBW}
	echo "==> Building spectrogram"
	sox -S "${OUT_DIR}/$(basename "$1" .flac).flac" -n spectrogram -o "${SPECTRO_DIR}/$(basename "$1" .flac).png"
}

# -----------------------------------------------------------------------------
lpf_dir()
{
	for i in "$1"/*.flac
	do 
		lpf_file "$i"
	done
}

# -----------------------------------------------------------------------------
echo_bold()
{
	ECHO_STRING=$1
	echo -e "\e[1m${ECHO_STRING}\e[0m"
}

# -***************************************************************************-
if [[ $# -lt 4 ]]; then
	usage
	exit 1
fi

# -***************************************************************************-
while [ "$1" != "" ]; do
	case $1 in
		-f | --frequency )
			if [ "$2" != "" ]; then
				FREQ=$2
				shift
			else
				usage
				exit 2
			fi
			;;
		-t | --tbw )
			if [ "$2" != "" ]; then
				TBW=$2
				shift
			else
				usage
				exit 2
			fi    
			;;
		-h | --help )           
			usage
			exit
			;;
		* )                     
			WORK_ITEM=$1
			;;
	esac
	shift
done

# -***************************************************************************-
OUT_DIR=LPF-${FREQ}
SPECTRO_DIR="${OUT_DIR}"/spectrograms

mkdir -p "${OUT_DIR}"
mkdir -p "${SPECTRO_DIR}"

# -***************************************************************************-
if [[ -d "${WORK_ITEM}" ]]; then
	lpf_dir "${WORK_ITEM}"
elif [[ -f "${WORK_ITEM}" ]]; then
	lpf_file "${WORK_ITEM}"
else
	echo "The given path is not directory nither file: ${WORK_ITEM}"
	usage
fi

