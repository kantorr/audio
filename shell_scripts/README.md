# audio

Several tools for audio manipulation.  
Each tool has usage information that might be issued by running a tool without arguments.  

## `dsf2flac`

Convert `DSF` files to `FLAC`.  

### Usage

```
dsf2flac: ver.1.1: Convert files in required folder from DSF (DSD) to FLAC format
Usage:
dsf2flac.sh <folder> [-m --multi]
Where:
 folder     - The desired folder contains files to be converted
 -m --multi - Downmix surround 5.1 multi-track to stereo.
              Combining as 0.5*Center + 0.707*Front + 0.707*Back + 0.5*LFE
 Converted files will be placed in FLAC-RAW folder inside the required one
 ```

## `extract`

Extract audio-tracks from video container.

### Usage

```
extract: ver.1.1: Extract audio tracks from multimedia (video) container
Usage:
extract.sh <video_container> -t track_number -c audio_codec -b bitrate -d dynamic_range -m downmix -s suffix
Where:
  video_container - The video-file containing the desired audio-track
     track_number - The number of desired audio-track: 1 or 2 or 1,2 (-t --track)
                    For multiple track extraction specify for example: 1,2,5,7,16
                    For extraction of all audio tracks specify: a or A
      audio_codec - The name of audio codec: ffmpeg -codecs (-c --codec)
          bitrate - The desired bitrate: 320k for MP3 or 529k for AAC (-b --bitrate)
    dynamic_range - The desired dynamic-range: 16 or 32 bits (-d --dynrange)
          downmix - The down-mix the multichannel track to stereo: 6 or 8 (-m --multi)
           suffix - The suffix for output file: Rus,Ukr,Eng (-s --suffix)
```

### Extract three 5.1 surround tracks through down-mixing to stereo

Extract three audio tracks from the file: `video-file.mkv`

```
extract.sh video-file.mkv -t 2,4,5 -c flac -m 6
```

Three files will be created near the `video-file.mkv` file, in the same directory:  
- video-file.02.flac
- video-file.04.flac
- video-file.05.flac

All three files will contain down-mixed stereo audio tracks.

### Extract all audio track as 5.1 surround through down-mixing them to stereo

Extract all audio tracks from the `video-file.mkv` file:

```
extract.sh video-file.mkv -t a -c flac -m 6
```

One file will be created near the `video-file.mkv` file, in the same directory: `video-file-audio-tracks.mkv`.  
All audio tracks will be converted to down-mixed stereo in `FLAC` format.  

### Copy all audio tracks

Copy all audio tracks from the `video-file.mkv` file:

```
extract.sh video-file.mkv -t a -c copy
```

One file will be created near the `video-file.mkv` file, in the same directory: `video-file-audio-tracks.mkv`.  
This file will contain exact copies of all audio tracks from the `video-file.mkv` file, without the video track.  


## `lpf`

Low Pass Filter.

### Usage

```
lpf: ver.1.0: Implement Low Pass Filter to files in FLAC format with desired sample rate
Usage:
lpf.sh <folder> -f freg -t range
Where:
 folder - The desired folder contains files to be converted
   freq - The basic frequency for the low pass filter
  range - The range of frequencies for the low pass filter
 Converted files will be placed in LPF-xx folder inside the required one
```

## `normalize`

Audio normalization up to 0dB.

### Usage

```
normalize: ver.1.0: Normalize audio file on 0 dB
Usage:
normalize.sh <file_path> <folder>
Where:
  file_path - The desired file to be normalized
     folder - The desired folder contains files to be normalized
  Normalized files will be placed in NORMALIZED folder inside the required one
```

## `normalize-ffmpeg`

Audio normalization with following parameters:  
- Normalization algorithm: [EBU R 128](https://en.wikipedia.org/wiki/EBU_R_128)
- Maximal peak level: `+0.0 dB`
- Audio Encoder: [AAC](https://en.wikipedia.org/wiki/Advanced_Audio_Coding)
- Audio Bitrate: `529k` `VBR`
- Audio Sample Rate: `48 kHz`

### Usage

```
normalize-ffmpeg: ver.1.0: Normalize audio file by ffmpeg-normalize utility
Usage:
normalize-ffmpeg.sh <file_path> <folder>
Where:
  file_path - The desired file to be normalized
     folder - The desired folder contains files to be normalized
  Normalized files will be placed in NORMALIZED folder inside the required one
  Normalization will be taken by EBU R128 algorithm with following parameters:
  Maximal peak level: +0.0 dB, Encoder: AAC, Bitrate: 529k VBR, Sample Rate: 48 kHz
```

## `rate`

Change audio-rate of desired audio-files.

### Usage

```
rate: ver.1.0: Bring audio files in desired directory to desired sample-rate
Usage:
rate.sh <folder> <sample_rate>
Where:
      folder - The desired folder contains files to be converted
 sample_rate - The desired sample-rate (44100, 48000, 48k, 88200, 96000, 96k)
 Converted files will be placed in RATE-xx folder inside the required one
```

## `spectogram`

Build a visual spectogram over desired audio-files.

### Usage

```
spectrogram: ver.1.0: Build a spectrogram over all audio files in desired folder
Usage:
spectrogram.sh <folder>
Where:
 folder - The desired folder contains audio files
 Spectrograms will be builded as PNG files and will be located in 'spectrograms'
 folder inside the required one
```

## Dependencies

### [Python 3.x](https://www.python.org)

Install `Python 3.x` version:  
```bash
> sudo apt install python3
```

### [ffmpeg](https://ffmpeg.org)

Install ffmpeg from your distribution:  
```bash
> sudo apt install ffmpeg
```
The latest version might be installed from http://ffmpeg.org/, it should be added in the `$PATH` environment.  


### [SoX](http://sox.sourceforge.net)

[SoX](http://sox.sourceforge.net) installation is required.  
On `Linux` or `mac OS X` install it by following command:  
```bash
> sudo apt install sox
```

### [ffmpeg-normalize](https://github.com/slhck/ffmpeg-normalize)

[ffmpeg-normalize](https://pypi.org/project/ffmpeg-normalize/) installation required as well.  
Install it on Linux or mac OS X by following command:  
```bash
> pip install ffmpeg-normalize
```

Consider to use virtual environments to avoid mess with different python installation on your machine.   
```bash
> pip install virtualenv

# Python 3
> python3 -m venv audio-normalize
> source audio-normalize/bin/activate
(audio-normalize) > pip install ffmpeg-normalize
(audio-normalize) > deactivate
>
```

---

## License

The [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html) (GPLv3)

Audio manipulation tools.  
Copyright (C) 2020  Ruslan Kantorovych  

This set of programs is free software: you can redistribute it   
and/or modify it under the terms of the GNU General Public License   
as published by the Free Software Foundation, version 3 of the  
License (GPLv3).  
  
This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU General Public License for more details.  
  
You should have received a copy of the GNU General Public License  
along with this program.  If not, see https://www.gnu.org/licenses/.  

