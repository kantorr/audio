#!/bin/bash

EXE_NAME=spectrogram
SPECTRO_DIR=spectrograms

echo "${EXE_NAME}: ver.1.0: Build a spectrogram over all audio files in desired folder"

# -----------------------------------------------------------------------------
usage()
{
	echo "Usage:"
	echo "${EXE_NAME}.sh <folder>"
	echo "Where:"
	echo " folder - The desired folder contains audio files"
	echo " Spectrograms will be builded as PNG files and will be located in '${SPECTRO_DIR}'"
    echo " folder inside the required one"
}

# -----------------------------------------------------------------------------
build_spectrogram()
{
	# Differentiate the path of the source file
	SRC_FILE=$(basename -- "${1}")
	SRC_FILE_EXT="${SRC_FILE##*.}"
	SRC_FILE_NAME="${SRC_FILE%.*}"

	echo "===> Building spectrogram for: ${1}"
	sox "${1}" -n spectrogram -o "${SPECTRO_DIR}/${SRC_FILE_NAME}.png"
}

# -----------------------------------------------------------------------------
build_spectrogram_over_dir()
{
	echo "==> Building spectrogram over folder: ${1}"

	SUPPORTED_EXT=( "flac" "mp3" "ogg" "vorbis" "wav")

	for ext in ${SUPPORTED_EXT[*]}
	do
		mkdir -p "${SPECTRO_DIR}"
	
		for i in "${1}"/*.$ext
		do 
			if [ -f "$i" ]; then
				build_spectrogram "${i}"
			fi
		done
	done
}

# -***************************************************************************-
if [[ $# -gt 0 ]]; then
	if [[ -d "${1}" ]]; then
		build_spectrogram_over_dir "${1}"

	elif [[ -f "${1}" ]]; then
		mkdir -p "${SPECTRO_DIR}"
		build_spectrogram "${1}"

	else
		echo "The given path is not directory nither file: ${1}"
		usage
	fi
else
	usage
fi

