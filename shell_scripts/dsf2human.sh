#!/bin/bash

# Hello,
# I did recordings from older tape deck at 48 kHz sample rate and would like to 
# filter (low-pass) frequencies above e.g.  approximately 18 kHz. 
# Using SoX, is it OK to use:
#     sox.exe in.flac out.flac sinc -18000
# or  rather
#     sox.exe in.flac out.flac lowpass 18000 (which has a IIR Butterworth filter) ?
# The first one seems to be more advanced and has no significant artifacts as far
# I have tried it. If I would like to use slower roll off filter (which I prefer) 
# than default 5 per cent, can it be modified to the following ?
#     sox.exe in.flac out.flac sinc -18000 -t 3000
# Thanks for opinions.
# Jan
# -----
# Yes, that gives you a transition-band from 16500 to 19500 Hz (and 6dB down @ 18kHz, 
# the mid-point).
# Use the: sox --plot option (with Gnuplot or Octave) to check that the filter 
# response (either sinc or lowpass) is as expected.

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------

# sinc [−a att|−b beta] [−p phase|−M|−I|−L] [−t tbw|−n taps] [freqHP]
# [−freqLP [−t tbw|−n taps]]

# Apply a sinc kaiser-windowed low-pass, high-pass, band-pass, or band-reject filter to the signal. The freqHP and freqLP parameters give the frequencies of the 6dB points of a high-pass and low-pass filter that may be invoked individually, or together. If both are given, then freqHP less than freqLP creates a band-pass filter, freqHP greater than freqLP creates a band-reject filter. For example, the invocations

# sinc 3k
# sinc -4k
# sinc 3k-4k
# sinc 4k-3k

# create a high-pass, low-pass, band-pass, and band-reject filter respectively.

# The default stop-band attenuation of 120dB can be overridden with −a; alternatively, the kaiser-window ‘beta’ parameter can be given directly with −b.

# The default transition band-width of 5% of the total band can be overridden with −t (and tbw in Hertz); alternatively, the number of filter taps can be given directly with −n.

# If both freqHP and freqLP are given, then a −t or −n option given to the left of the frequencies applies to both frequencies; one of these options given to the right of the frequencies applies only to freqLP.

# The −p, −M, −I, and −L options control the filter’s phase response; see the rate effect for details.

# You can view the filter command structure using these help commands.

# sox --help-effect bandpass
# sox --help-effect bandreject
# sox --help-effect highpass
# sox --help-effect lowpass
# sox --help-effect sinc

echo "dsf2human: ver.1.1"
echo "Convert from DSF (DSD) to 24-bit FLAC format in 48 or 44.1 kHz sample-rate."
echo "The Band-Pass filter of human hearing frequencies range is apllyied."

# -----------------------------------------------------------------------------
usage()
{
	echo ""
	echo "Usage:"
	echo "dsf2human.sh <folder> [-m, --multi] [-s, --short-band] [-v, --verbose] [-r, --rate]"
	echo "Where:"
    echo "    folder"
    echo "        The desired folder contains files to be converted"
    echo ""
	echo "    -h, -H, --help"
	echo "        Display this help information."
	echo
	echo "    -m -M --multi"
	echo "        Downmix surround 5.1 multi-track to stereo."
	echo "        Combining as 0.5*Center + 0.707*Front + 0.707*Back + 0.5*LFE"
    echo ""
	echo "    -s, -S, --short-band"
	echo "        Appy Band-Pass filter with shorten band based on 24 Hz and 16 kHz."
	echo "        The default Band-Pass filter is based on 20 Hz and 20 kHz."
	echo
	echo "    -r, -R, --rate rate_value"
	echo "        Convert to desired rate_value (-r 44100, --rate 48000)."
	echo "        The default rate is 48 kHz."
	echo
	echo "    -v, -V, --verbose"
	echo "        Verbose outputll."
	echo
	echo " Converted files will be placed in ${OUT_DIR} folder inside the required one"
}

# -----------------------------------------------------------------------------
# Applying Band-Pass Filter of human hearing range and save with 24-bits in 44100 sample-rate.
# Low-Pass filter will cut at 20kHz with bandwith of 12kHz.
# High-Pass filter will cut at 24Hz with bandwith of 12Hz.
convert_file_44()
{
	if [[ -f "${1}" ]]; then
		# Differentiate the path of the source file
		SRC_FILE=$(basename -- "${1}")
		SRC_FILE_EXT="${SRC_FILE##*.}"
		SRC_FILE_NAME="${SRC_FILE%.*}"

		#      filter tbwHP freq   tbwLP
		BPF_SOX="sinc ${FREQ_BAND}"

		echo "=> Converting to 24-44.1 through BPF: ${1}"
		if [ ${VERBOSE_SOX} ]; then
			echo "===> Audio-Filter: ${AUDIO_FILTER}"
		fi
		# ffmpeg -i "${1}" ${QUITE_FFMPEG} -af "${AUDIO_FILTER}" -f wav -acodec pcm_s24le -ar 176400 pipe:1 | sox ${VERBOSE_SOX} -t wav --ignore-length - --compression 0 "${OUT_DIR}/${SRC_FILE_NAME}.flac" ${BPF_SOX}

		ffmpeg -i "${1}" ${QUITE_FFMPEG} -af "${AUDIO_FILTER}" -f wav -acodec pcm_s24le -ar 88200 pipe:1 | sox ${VERBOSE_SOX} -t wav --ignore-length - --compression 0 "${OUT_DIR}/${SRC_FILE_NAME}.flac"

		if [ ${VERBOSE_SOX} ]; then
			echo "==> Building intermediate 88.2 kHz spectrogram"
		fi
		mkdir -p "${SPECTRO_DIR}-88.2"
		sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}.flac" -n spectrogram -o "${SPECTRO_DIR}-88.2"/"${SRC_FILE_NAME}.png"

		if [ ${VERBOSE_SOX} ]; then
			echo "==> Applying Band-Pass Filter: ${FREQ_BAND}"
		fi
		sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}.flac" --compression 0 "${OUT_DIR}/${SRC_FILE_NAME}-bpf.flac" ${BPF_SOX}

		if [ ${VERBOSE_SOX} ]; then
			echo "==> Down-scaling to sample-rate of 44.1 kHz"
		fi
		sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}-bpf.flac" --compression 5 "${OUT_DIR}/${SRC_FILE_NAME}.flac" rate -v -I -b 90 44100
		# sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}-bpf.flac" --compression 5 "${OUT_DIR}/${SRC_FILE_NAME}.flac" rate -v -s -a 44100
		rm -f "${OUT_DIR}/${SRC_FILE_NAME}-bpf.flac"

		if [ ${VERBOSE_SOX} ]; then
			echo "==> Building 44.1 kHz spectrogram"
		fi
		sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}.flac" -n spectrogram -o "${SPECTRO_DIR}"/"${SRC_FILE_NAME}.png"
	fi
}

# -----------------------------------------------------------------------------
# Applying Band-Pass Filter of human hearing range and save with 24-bits in 48000 sample-rate.
# Low-Pass filter will cut at 20kHz with bandwith of 12kHz.
# High-Pass filter will cut at 24Hz with bandwith of 12Hz.
convert_file_48()
{
	if [[ -f "${1}" ]]; then
		# Differentiate the path of the source file
		SRC_FILE=$(basename -- "${1}")
		SRC_FILE_EXT="${SRC_FILE##*.}"
		SRC_FILE_NAME="${SRC_FILE%.*}"

		#      filter tbwHP freq   tbwLP
		BPF_SOX="sinc ${FREQ_BAND}"

		echo "=> Converting to 24-48 through BPF: ${1}"
		if [ ${VERBOSE_SOX} ]; then
			echo "===> Audio-Filter: ${AUDIO_FILTER}"
		fi
		# ffmpeg -i "${1}" ${QUITE_FFMPEG} -af "${AUDIO_FILTER}" -f wav -acodec pcm_s24le -ar 176400 pipe:1 | sox ${VERBOSE_SOX} -t wav --ignore-length - --compression 0 "${OUT_DIR}/${SRC_FILE_NAME}.flac" ${BPF_SOX}

		ffmpeg -i "${1}" ${QUITE_FFMPEG} -af "${AUDIO_FILTER}" -f wav -acodec pcm_s24le -ar 96000 pipe:1 | sox ${VERBOSE_SOX} -t wav --ignore-length - --compression 0 "${OUT_DIR}/${SRC_FILE_NAME}.flac"

		if [ ${VERBOSE_SOX} ]; then
			echo "==> Building intermediate 96 kHz spectrogram"
		fi
		mkdir -p "${SPECTRO_DIR}-96"
		sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}.flac" -n spectrogram -o "${SPECTRO_DIR}-96"/"${SRC_FILE_NAME}.png"

		# if [ ${VERBOSE_SOX} ]; then
		# 	echo "==> Normalizing gain to -2 dB"
		# fi
		# sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}.flac" "${OUT_DIR}/${SRC_FILE_NAME}-norm.flac" gain -n -2

		if [ ${VERBOSE_SOX} ]; then
			echo "==> Applying Band-Pass Filter: ${FREQ_BAND}"
		fi
		sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}.flac" --compression 0 "${OUT_DIR}/${SRC_FILE_NAME}-bpf.flac" ${BPF_SOX}

		if [ ${VERBOSE_SOX} ]; then
			echo "==> Down-scaling to sample-rate of 48 kHz"
		fi
		sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}-bpf.flac" --compression 5 "${OUT_DIR}/${SRC_FILE_NAME}.flac" rate -v -I -b 90 48000
		# sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}-bpf.flac" --compression 5 "${OUT_DIR}/${SRC_FILE_NAME}.flac" rate -v -s -a 48000
		rm -f "${OUT_DIR}/${SRC_FILE_NAME}-bpf.flac"

		if [ ${VERBOSE_SOX} ]; then
			echo "==> Building 48 kHz spectrogram"
		fi
		sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}.flac" -n spectrogram -o "${SPECTRO_DIR}"/"${SRC_FILE_NAME}.png"
	fi
}

# -----------------------------------------------------------------------------
convert_dir()
{
	SUPPORTED_EXT=( "dsf" "dff" "dts" "wav")

	for ext in ${SUPPORTED_EXT[*]}
	do
		mkdir -p "${OUT_DIR}"
		mkdir -p "${SPECTRO_DIR}"
	
		for i in "${1}"/*.$ext
		do 
			if [ -f "$i" ]; then
				if [ ${RATE} == "48000" ]; then
					convert_file_48 "${i}"
				else
					convert_file_44 "${i}"
				fi
			fi
		done
	done
}

# *****************************************************************************

OUT_DIR=FLAC-HUMAN
SPECTRO_DIR=$OUT_DIR/spectrograms
QUITE_FFMPEG="-v quiet"
AUDIO_FILTER="volumedetect"
FREQ_BAND="-t 12 20-20k -t 12k"
RATE="48000"

while [ "$1" != "" ]; do
	case $1 in
		-m | -M | --multi)
			AUDIO_FILTER="pan=stereo|FL=0.5*FC+0.707*FL+0.707*BL+0.5*LFE|FR=0.5*FC+0.707*FR+0.707*BR+0.5*LFE, volumedetect"
			;;
		-s | -S | --short-band )
			FREQ_BAND="-t 12 24-16k -t 12k"
			;;
		-r | --rate )
			if [ "$2" != "" ]; then
				RATE=$2
				shift
			else
				usage
				exit 2
			fi
			;;
		-v | -V | --verbose)
			VERBOSE_SOX=-S
			QUITE_FFMPEG=""
			;;
		-h | -H | --help )           
			usage
			exit
			;;
		* )                     
			WORK_ITEM=$1
			;;
	esac
	shift
done

if [[ -d "${WORK_ITEM}" ]]; then
	convert_dir "${WORK_ITEM}"

elif [[ -f "${WORK_ITEM}" ]]; then
	mkdir -p "${SPECTRO_DIR}"
	convert_file "${WORK_ITEM}"

else
	echo "The given path is not directory nither file: ${1}"
	usage
	exit
fi
