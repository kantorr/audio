#!/bin/bash

echo "hpf: ver.1.0: Apply High-Pass Filter to files in FLAC format with desired base-frequency."

# -----------------------------------------------------------------------------
usage()
{
	echo "Usage:"
	echo "hpf.sh <folder> -f freg -t range"
	echo "Where:"
	echo " folder - The desired folder contains files to be converted"
	echo "   freq - The basic frequency for the high-pass filter"
	echo "  range - The range of frequencies for the high-pass filter"
	echo " Converted files will be placed in HPF-xx folder inside the required one"
}

# -----------------------------------------------------------------------------
hpf_file()
{
	echo "==> High-Pass Filtering at frequency ${FREQ} Hz in range of ${TBW} Hz x 2: $1"
	sox ${VERBOSE} "$1" --compression 5 "${OUT_DIR}/$(basename "$1" .flac).flac" sinc -t ${TBW} ${FREQ}
	echo "===> Building spectrogram"
	sox ${VERBOSE} "${OUT_DIR}/$(basename "$1" .flac).flac" -n spectrogram -o "${SPECTRO_DIR}/$(basename "$1" .flac).png"
}

# -----------------------------------------------------------------------------
hpf_dir()
{
	for i in "$1"/*.flac
	do 
		hpf_file "$i"
	done
}

# -----------------------------------------------------------------------------
echo_bold()
{
	ECHO_STRING=$1
	echo -e "\e[1m${ECHO_STRING}\e[0m"
}

# -***************************************************************************-
if [[ $# -lt 4 ]]; then
	usage
	exit 1
fi

# -***************************************************************************-
while [ "$1" != "" ]; do
	case $1 in
		-f | --frequency )
			if [ "$2" != "" ]; then
				FREQ=$2
				shift
			else
				usage
				exit 2
			fi
			;;
		-t | --tbw )
			if [ "$2" != "" ]; then
				TBW=$2
				shift
			else
				usage
				exit 2
			fi    
			;;
		-v | --verbose )           
			VERBOSE=-S
			;;
		-h | --help )           
			usage
			exit
			;;
		* )                     
			WORK_ITEM=$1
			;;
	esac
	shift
done

# -***************************************************************************-
OUT_DIR=HPF-${FREQ}
SPECTRO_DIR="${OUT_DIR}"/spectrograms

mkdir -p "${OUT_DIR}"
mkdir -p "${SPECTRO_DIR}"

# -***************************************************************************-
if [[ -d "${WORK_ITEM}" ]]; then
	hpf_dir "${WORK_ITEM}"
elif [[ -f "${WORK_ITEM}" ]]; then
	hpf_file "${WORK_ITEM}"
else
	echo "The given path is not directory nither file: ${WORK_ITEM}"
	usage
fi

