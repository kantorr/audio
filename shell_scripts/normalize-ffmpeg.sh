#!/bin/bash

echo "normalize-ffmpeg: ver.1.0: Normalize audio file by ffmpeg-normalize utility"

OUT_DIR=NORMALIZED

# -----------------------------------------------------------------------------
usage()
{
	echo "Usage:"
	echo "normalize-ffmpeg.sh <file_path> <folder>"
	echo "Where:"
	echo "  file_path - The desired file to be normalized"
	echo "     folder - The desired folder contains files to be normalized"
	echo "  Normalized files will be placed in ${OUT_DIR} folder inside the required one"
	echo "  Normalization will be taken by EBU R128 algorithm with following parameters:"
	echo "  Maximal peak level: +0.0 dB, Encoder: AAC, Bitrate: 529k VBR, Sample Rate: 48 kHz"
}

# -----------------------------------------------------------------------------
normalize_file()
{
	SRC_FILE=$(basename -- "$1")
	SRC_FILE_EXT="${SRC_FILE##*.}"
	SRC_FILE_NAME="${SRC_FILE%.*}"

	if [[ $SRC_FILE_EXT == "mkv" ]]; then 
		EXT=$SRC_FILE_EXT
	else
		EXT=m4a
	fi

	NORM_FILE="${OUT_DIR}/${SRC_FILE_NAME}.norm.${EXT}"
	OUT_FILE="${OUT_DIR}/${SRC_FILE_NAME}.final.${EXT}"

	echo "==> Normalizing: ${1} ==> ${NORM_FILE}"
	ffmpeg-normalize "${1}" -c:a aac -b:a 529k -ar 48000 -nt ebu -tp +0.0 -p -pr -o "${NORM_FILE}"
	echo "==> Converting normalized: ${NORM_FILE} ==> ${OUT_FILE}" 
	# ffmpeg -i ${1} -map 0:g -map 0:v -map 0:s -i ${NORM_FILE} -map 1:a -c copy -map_metadata 0:g
}

# -----------------------------------------------------------------------------
normalize_dir()
{
	mkdir -p "${OUT_DIR}"
	
	echo "==> Normalizing directory: ${1}"

	for i in $1/*
	do 
		normalize_file "$i"
	done
}

# -***************************************************************************-
if [[ $# -gt 0 ]]; then
	if [[ -d "${1}" ]]; then
		normalize_dir "${1}"

	elif [[ -f "${1}" ]]; then
		mkdir -p "${OUT_DIR}"
		normalize_file "${1}"

	else
		echo "The given path is not directory nither file: ${1}"
		usage
	fi
else
	usage
fi
