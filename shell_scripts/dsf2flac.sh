#!/bin/bash

# Hello,
# I did recordings from older tape deck at 48 kHz sample rate and would like to 
# filter (low-pass) frequencies above e.g.  approximately 18 kHz. 
# Using SoX, is it OK to use:
#     sox.exe in.flac out.flac sinc -18000
# or  rather
#     sox.exe in.flac out.flac lowpass 18000 (which has a IIR Butterworth filter) ?
# The first one seems to be more advanced and has no significant artifacts as far
# I have tried it. If I would like to use slower roll off filter (which I prefer) 
# than default 5 per cent, can it be modified to the following ?
#     sox.exe in.flac out.flac sinc -18000 -t 3000
# Thanks for opinions.
# Jan
# -----
# Yes, that gives you a transition-band from 16500 to 19500 Hz (and 6dB down @ 18kHz, 
# the mid-point).
# Use the: sox --plot option (with Gnuplot or Octave) to check that the filter 
# response (either sinc or lowpass) is as expected.

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------

# sinc [−a att|−b beta] [−p phase|−M|−I|−L] [−t tbw|−n taps] [freqHP]
# [−freqLP [−t tbw|−n taps]]

# Apply a sinc kaiser-windowed low-pass, high-pass, band-pass, or band-reject filter to the signal. The freqHP and freqLP parameters give the frequencies of the 6dB points of a high-pass and low-pass filter that may be invoked individually, or together. If both are given, then freqHP less than freqLP creates a band-pass filter, freqHP greater than freqLP creates a band-reject filter. For example, the invocations

# sinc 3k
# sinc -4k
# sinc 3k-4k
# sinc 4k-3k

# create a high-pass, low-pass, band-pass, and band-reject filter respectively.

# The default stop-band attenuation of 120dB can be overridden with −a; alternatively, the kaiser-window ‘beta’ parameter can be given directly with −b.

# The default transition band-width of 5% of the total band can be overridden with −t (and tbw in Hertz); alternatively, the number of filter taps can be given directly with −n.

# If both freqHP and freqLP are given, then a −t or −n option given to the left of the frequencies applies to both frequencies; one of these options given to the right of the frequencies applies only to freqLP.

# The −p, −M, −I, and −L options control the filter’s phase response; see the rate effect for details.

# You can view the filter command structure using these help commands.

# sox --help-effect bandpass
# sox --help-effect bandreject
# sox --help-effect highpass
# sox --help-effect lowpass
# sox --help-effect sinc

# -----------------------------------------------------------------------------
usage()
{
	echo "Usage:"
	echo "dsf2flac.sh <folder> [-m --multi]"
	echo "Where:"
    echo " folder     - The desired folder contains files to be converted"
	echo " -m --multi - Downmix surround 5.1 multi-track to stereo."
	echo "              Combining as 0.5*Center + 0.707*Front + 0.707*Back + 0.5*LFE"
	echo " Converted files will be placed in ${OUT_DIR} folder inside the required one"
}

# -----------------------------------------------------------------------------
convert_file()
{
	if [[ -f "${1}" ]]; then
		# Differentiate the path of the source file
		SRC_FILE=$(basename -- "${1}")
		SRC_FILE_EXT="${SRC_FILE##*.}"
		SRC_FILE_NAME="${SRC_FILE%.*}"

		echo "==> Converting file: ${1}"
		if [ ${VERBOSE_SOX} ]; then
			echo "Audio-Filter: ${AUDIO_FILTER}"
		fi
		ffmpeg -i "${1}" ${QUITE_FFMPEG} -af "${AUDIO_FILTER}" -f wav -acodec pcm_s24le -ar 176400 pipe:1 | sox ${VERBOSE_SOX} -t wav --ignore-length - --compression 5 "${OUT_DIR}/${SRC_FILE_NAME}.flac" sinc -48k -t 26k 
		
		echo "==> Building spectrogram"
		sox ${VERBOSE_SOX} "${OUT_DIR}/${SRC_FILE_NAME}.flac" -n spectrogram -o "${SPECTRO_DIR}"/"${SRC_FILE_NAME}.png"
	fi
}

# -----------------------------------------------------------------------------
convert_dir()
{
	mkdir -p "${OUT_DIR}"
	mkdir -p "${SPECTRO_DIR}"

	for i in "${1}"/*.dsf
	do 
		convert_file "${i}"
	done

	for i in "${1}"/*.dff
	do 
		convert_file "${i}"
	done
}

# *****************************************************************************
WORK_DIR=$1
OUT_DIR=FLAC-RAW
SPECTRO_DIR=$OUT_DIR/spectrograms

echo "dsf2flac: ver.1.1: Convert files in required folder from DSF (DSD) to FLAC format"

case $2 in
	-m | --multi)
		AUDIO_FILTER="pan=stereo|FL=0.5*FC+0.707*FL+0.707*BL+0.5*LFE|FR=0.5*FC+0.707*FR+0.707*BR+0.5*LFE, volumedetect"
		;;
	-v | --verbose)
		VERBOSE_SOX=-S
		QUITE_FFMPEG=""
		;;
	-h | --help )           
		usage
		exit
		;;
	*)
		AUDIO_FILTER="volumedetect"
		;;
esac

if [[ $# -gt 0 ]]; then
	if [[ -d "${1}" ]]; then
		convert_dir "${1}"

	elif [[ -f "${1}" ]]; then
		mkdir -p "${SPECTRO_DIR}"
		convert_file "${1}"

	else
		echo "The given path is not directory nither file: ${1}"
		usage
		exit
	fi
else
	usage
	exit
fi
