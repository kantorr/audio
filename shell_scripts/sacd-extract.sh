
SACD_EXTRACTOR="~/Music/sacd_extract-0.3.9.3-71-macOS/sacd_extract"

# *****************************************************************************

OUT_DIR=FLAC-HUMAN
SPECTRO_DIR=$OUT_DIR/spectrograms
QUITE_FFMPEG="-v quiet"
AUDIO_FILTER="volumedetect"
FREQ_BAND="-t 12 20-20k -t 12k"
RATE="48000"

while [ "$1" != "" ]; do
	case $1 in
		-m | -M | --multi)
			MULTY=1
			;;
		-s | -S | --short-band )
			SHORT_HUMAN_BAND=1
			;;
		-r | --rate )
			if [ "$2" != "" ]; then
				RATE=$2
				shift
			else
				usage
				exit 2
			fi
			;;
		-v | -V | --verbose)
			VERBOSE=1
			;;
		-h | -H | --help )           
			usage
			exit
			;;
		* )                     
			WORK_ITEM=$1
			;;
	esac
	shift
done


find "$PWD" -type f -name "*.in"
