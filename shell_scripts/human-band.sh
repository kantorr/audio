#!/bin/bash

echo "human-band: ver.1.1"
echo "    Apply a sinc kaiser-windowed Band-Pass Filter in human hearing frequencies range"
echo "    on to files in FLAC format with sample-rate based on 44.1kHz (88.2, 176.4, 352.8 kHz)."
echo "    Don't use it with sample-rate based on 48kHz like 48, 96, 192 kHz!"

OUT_DIR=HUMAN-BAND
FREQ_BAND="-t 12 20-20k -t 12k"

# -----------------------------------------------------------------------------
usage()
{
	echo "Usage:"
	echo "human-band-48.sh <folder> [-s, -h, -v]"
	echo "Where:"
	echo "      folder - The desired folder contains files to be converted"
	echo
	echo "      -s, -S, --short-band"
	echo "                Appy Band-Pass filter with shorten band based on 24 Hz and 16 kHz."
	echo "                The default Band-Pass filter is based on 20 Hz and 20 kHz."
	echo
	echo "      -h, -H, --help"
	echo "                Display this help information."
	echo
	echo "      -v, -V, --verbose"
	echo "                Verbose outputll."
	echo
	echo " Converted files will be placed in ${OUT_DIR} folder inside the required one"
}

# -----------------------------------------------------------------------------
echo_bold()
{
	ECHO_STRING=$1
	echo -e "\e[1m${ECHO_STRING}\e[0m"
}

# -----------------------------------------------------------------------------
build_spectrogram()
{
	local IN_FILE="${OUT_DIR}/$(basename "$1" .flac).flac"
	local OUT_FILE="${SPECTRO_DIR}/$(basename "$1" .flac).png"
	if [ ${VERBOSE} ]; then
		echo "===> Building spectrogram"
	fi
	sox ${VERBOSE} "${IN_FILE}" -n spectrogram -o "${OUT_FILE}"
}

# -----------------------------------------------------------------------------
process_file()
{
	echo "==> Processing file: ${1}"
	local OUT_FILE="${OUT_DIR}/$(basename "$1" .flac).flac"
	local OUT_FILE_BPF="${OUT_DIR}/$(basename "$1" .flac)-bpf.flac"
	if [ ${VERBOSE} ]; then
		echo "===> Converting to sample-rate 88200 Hz"
	fi
	sox ${VERBOSE} --guard "$1" --compression 0 "${OUT_FILE}" rate -v -s -a 88200
	if [ ${VERBOSE} ]; then
		echo "===> Human Band-Pass filtering based on: ${FREQ_BAND}"
	fi
	sox ${VERBOSE} --guard "${OUT_FILE}" --compression 0 "${OUT_FILE_BPF}" sinc ${FREQ_BAND}
	if [ ${VERBOSE} ]; then
		echo "===> Downscaling to sample-rate 44100 Hz"
	fi
	sox ${VERBOSE} --guard "${OUT_FILE_BPF}" --compression 5 "${OUT_FILE}" rate -v -I -b 90 44100
	rm -f "${OUT_FILE_BPF}"

	build_spectrogram "$1"
}

# -----------------------------------------------------------------------------
process_directory()
{
	SUPPORTED_EXT=( "flac" "wav")

	for ext in ${SUPPORTED_EXT[*]}
	do
		for i in "${1}"/*.$ext
		do 
			if [ -f "${i}" ]; then
				process_file "${i}"
			fi
		done
	done
}

# -----------------------------------------------------------------------------
make_dirs()
{
	mkdir -p "${OUT_DIR}"
	mkdir -p "${SPECTRO_DIR}"
}

# -***************************************************************************-
if [[ $# -lt 1 ]]; then
	usage
	exit 1
fi

# -***************************************************************************-
while [ "$1" != "" ]; do
	case $1 in
		-v | -V | --verbose )
			VERBOSE=-S
			;;
		-s | -S | --short-band )
			FREQ_BAND="-t 12 24-16k -t 12k"
			;;
		-h | -H | --help )           
			usage
			exit
			;;
		* )                     
			WORK_ITEM=$1
			;;
	esac
	shift
done

# -***************************************************************************-
SPECTRO_DIR="${OUT_DIR}"/spectrograms

# -***************************************************************************-
if [[ -d "${WORK_ITEM}" ]]; then
	make_dirs
	process_directory "${WORK_ITEM}"
elif [[ -f "${WORK_ITEM}" ]]; then
	make_dirs
	process_file "${WORK_ITEM}"
else
	echo "The given path is not directory nither file: ${WORK_ITEM}"
	usage
fi

