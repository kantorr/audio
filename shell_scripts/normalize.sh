#!/bin/bash

echo "normalize: ver.1.0: Normalize audio file on 0 dB"

OUT_DIR=NORMALIZED
SPECTRO_DIR="${OUT_DIR}"/spectrograms

# -----------------------------------------------------------------------------
usage()
{
	echo "Usage:"
	echo "normalize.sh <file_path> <folder>"
	echo "Where:"
	echo "  file_path - The desired file to be normalized"
	echo "     folder - The desired folder contains files to be normalized"
	echo "  Normalized files will be placed in ${OUT_DIR} folder inside the required one"
}

# -----------------------------------------------------------------------------
normalize_file()
{
	echo "==> Normalizing: ${1}"
	sox -S --guard "$1" --compression 5 "${OUT_DIR}/$(basename "$1" .flac).flac" norm
	echo "==> Building spectrogram"
	sox -S "${OUT_DIR}/$(basename "$1" .flac).flac" -n spectrogram -o "${SPECTRO_DIR}/$(basename "$1" .flac).png"
}

# -----------------------------------------------------------------------------
normalize_dir()
{
	mkdir -p "${OUT_DIR}"
	mkdir -p "${SPECTRO_DIR}"
	
	for i in $1/*.flac
	do 
		normalize_file "$i"
	done
}

# -***************************************************************************-
if [[ $# -gt 0 ]]; then
	if [[ -d "${1}" ]]; then
		normalize_dir "${1}"

	elif [[ -f "${1}" ]]; then
		mkdir -p "${OUT_DIR}"
		mkdir -p "${SPECTRO_DIR}"
		normalize_file "${1}"

	else
		echo "The given path is not directory nither file: ${1}"
		usage
	fi
else
	usage
fi
