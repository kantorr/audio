#!/bin/bash

echo "rate: ver.1.0: Bring audio files in desired directory to desired sample-rate"

OUT_DIR=RATE-xx

# -----------------------------------------------------------------------------
usage()
{
	echo "Usage:"
	echo "rate.sh <folder> <-r sample_rate> [-u -d -v]"
	echo "Where:"
	echo "      folder - The desired folder contains files to be converted"
	echo
	echo "      -r, --rate sample_rate[k]"
	echo "               Gives the sample rate in Hz (or kHz if appended with 'k') of the file."
	echo "               The given sample-rate may be as following: 44100, 48000, 48k, 88200, 96000, 96k, ..."
	echo 
	echo "      -d, --downscale"
	echo "                Make a downscaling, reduce the sample rate to the given one."
	echo 
	echo "      -u, --upscale"
	echo "                Make an upscaling, increase the sample rate to the given one."
	echo
	echo "      -v, --verbose"
	echo "                Verbose outputll."
	echo
	echo " Converted files will be placed in ${OUT_DIR} folder inside the required one"
}

# -----------------------------------------------------------------------------
echo_bold()
{
	ECHO_STRING=$1
	echo -e "\e[1m${ECHO_STRING}\e[0m"
}

# -----------------------------------------------------------------------------
build_spectrogram()
{
	local IN_FILE="${OUT_DIR}/$(basename "$1" .flac).flac"
	local OUT_FILE="${SPECTRO_DIR}/$(basename "$1" .flac).png"
	if [ ${VERBOSE} ]; then
		echo "===> Building spectrogram"
	fi
	sox ${VERBOSE} "${IN_FILE}" -n spectrogram -o "${OUT_FILE}"
}

# -----------------------------------------------------------------------------
# Take care of downscaling from 24-bit to 16-bit, in such a case dithering should be applyed.
# The noise-shaping filter `modified-e-weighted` is most suitable in terms of audibility, 
# according to this information: 
# - https://hydrogenaud.io/index.php?topic=111190.0
# - https://forums.stevehoffman.tv/threads/downsample-convert-hi-res-24-bit-to-16-bit-with-audacity-or-foobar2000.403739/
# - https://forums.stevehoffman.tv/threads/neil-young-on-digital-audio-youre-doing-it-wrong.348996/page-16#post-10411665
# 
downscale_file()
{
	local OUT_FILE="${OUT_DIR}/$(basename "$1" .flac).flac"
	echo "==> Downscaling to sample-rate ${RATE}: ${1}"
	sox ${VERBOSE} --guard "$1" --compression 5 "${OUT_FILE}" rate -v -I -b 90 ${RATE}
	# sox -S --guard "$1" "${OUT_DIR}/$(basename "$1" .flac).flac" rate -v -I ${RATE}
	# sox -S --guard "$i" "${OUT_DIR}/$(basename "$i" .flac).flac" rate -v -s -a ${RATE}
	# ffmpeg -i "$i" -ar ${RATE} "${OUT_DIR}/$(basename "$i" .flac).flac"
	build_spectrogram "$1"
}

# -----------------------------------------------------------------------------
upscale_file()
{
	local OUT_FILE="${OUT_DIR}/$(basename "$1" .flac).flac"
	echo "==> Upscaling to sample-rate ${RATE}: ${1}"
	sox ${VERBOSE} --guard "$1" --compression 5 "${OUT_FILE}" rate -v -s -a ${RATE}
	# ffmpeg -i "$i" -ar ${RATE} "${OUT_DIR}/$(basename "$i" .flac).flac"
	build_spectrogram "$1"
}

# -----------------------------------------------------------------------------
resample_file()
{
	if [[ DOWNSCALE -gt 0 ]]; then
		downscale_file "$1"
	elif [[ UPSCALE -gt 0 ]]; then
		upscale_file "$1"
	else
		echo "Decide what you want to do upscaling or downscaling!"
		usage
		exit 2
	fi
}

# -----------------------------------------------------------------------------
resample_directory()
{
	for i in "$1"/*.flac
	do 
		resample_file "$i"
	done
}

# -----------------------------------------------------------------------------
make_dirs()
{
	mkdir -p "${OUT_DIR}"
	mkdir -p "${SPECTRO_DIR}"
}

# -***************************************************************************-
if [[ $# -lt 3 ]]; then
	usage
	exit 1
fi

# -***************************************************************************-
UPSCALE=0
DOWNSCALE=0
RATE=0

while [ "$1" != "" ]; do
	case $1 in
		-d | --downscale )
			DOWNSCALE=1
			;;
		-u | --upscale )
			UPSCALE=1
			;;
		-v | --verbose )
			VERBOSE=-S
			;;
		-r | --rate )
			if [ "$2" != "" ]; then
				RATE=$2
				shift
			else
				usage
				exit 2
			fi    
			;;
		-h | --help )           
			usage
			exit
			;;
		* )                     
			WORK_ITEM=$1
			;;
	esac
	shift
done

# -***************************************************************************-
OUT_DIR=RATE-${RATE}
SPECTRO_DIR="${OUT_DIR}"/spectrograms

# -***************************************************************************-
if [[ -d "${WORK_ITEM}" ]]; then
	make_dirs
	resample_directory "${WORK_ITEM}"
elif [[ -f "${WORK_ITEM}" ]]; then
	make_dirs
	resample_file "${WORK_ITEM}"
else
	echo "The given path is not directory nither file: ${WORK_ITEM}"
	usage
fi

