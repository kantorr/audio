#!/bin/bash

echo "bpf: ver.1.0: Apply a sinc kaiser-windowed Band-Pass Filter on to files in FLAC format with desired frequencies."

# -----------------------------------------------------------------------------
usage()
{
	echo "Usage:"
	echo "bpf.sh <folder> [-t tbw] -f fregHP-freqLP  [-t tbwLP]"
	echo "Where:"
	echo "        folder - The desired folder contains files to be converted."
	echo " fregHP-freqLP - The frequencies of the 6dB points of a high-pass and low-pass filters. (-f, --freq)"
	echo "                 For the band-pass filter the freqHP should be less than freqLP."
	echo "           tbw - The transition band-width in Hertz. Default is 5% of the total band. (-t, --tbw)"
	echo "                 A -t option geven to the left of the frequencies applies to both HP and LP frequencies;"
	echo "                 given to the right of the frequencies applies only to LP frequency."
	echo " Converted files will be placed in LPF-xx folder inside the required one."
}

# -----------------------------------------------------------------------------
bpf_file()
{
	echo "==> Band-pass Filtering at frequency ${FREQ}: $1"
	# echo sox ${VERBOSE} "$1" "${OUT_DIR}/$(basename "$1" .flac).flac" sinc ${TBW_HP} ${FREQ} ${TBW_LP}
	sox ${VERBOSE} "$1" --compression 5 "${OUT_DIR}/$(basename "$1" .flac).flac" sinc ${TBW_HP} ${FREQ} ${TBW_LP}
	echo "===> Building spectrogram"
	sox ${VERBOSE} "${OUT_DIR}/$(basename "$1" .flac).flac" -n spectrogram -o "${SPECTRO_DIR}/$(basename "$1" .flac).png"
}

# -----------------------------------------------------------------------------
bpf_dir()
{
	for i in "$1"/*.flac
	do 
		bpf_file "$i"
	done
}

# -----------------------------------------------------------------------------
echo_bold()
{
	ECHO_STRING=$1
	echo -e "\e[1m${ECHO_STRING}\e[0m"
}

# -***************************************************************************-
if [[ $# -lt 4 ]]; then
	usage
	exit 1
fi

# -***************************************************************************-
FREQ=""

while [ "$1" != "" ]; do
	case $1 in
		-f | --frequency )
			if [ "$2" != "" ]; then
				FREQ=$2
				shift
			else
				usage
				exit 2
			fi
			;;
		-t | --tbw )
			if [ "$2" != "" ]; then
				if [[ ${FREQ} == "" ]]; then
					TBW_HP="-t $2"
					echo "Got tbwHP: ${TBW_HP}"
				else
					TBW_LP="-t $2"
					echo "Got tbwLP: ${TBW_LP}"
				fi
				shift
			else
				usage
				exit 2
			fi    
			;;
		-v | --verbose )
			VERBOSE=-S
			shift
			;;
		-h | --help )           
			usage
			exit
			;;
		* )                     
			WORK_ITEM=$1
			;;
	esac
	shift
done

# -***************************************************************************-
OUT_DIR=BPF-${FREQ}
SPECTRO_DIR="${OUT_DIR}"/spectrograms

mkdir -p "${OUT_DIR}"
mkdir -p "${SPECTRO_DIR}"

# -***************************************************************************-
if [[ -d "${WORK_ITEM}" ]]; then
	bpf_dir "${WORK_ITEM}"
elif [[ -f "${WORK_ITEM}" ]]; then
	bpf_file "${WORK_ITEM}"
else
	echo "The given path is not directory nither file: ${WORK_ITEM}"
	usage
fi

