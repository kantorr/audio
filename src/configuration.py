# configuration.py

import os
import sys
import json

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Config:
	'''
	General class for configuration purpose.

	Properties:
		file    - The configuration file
		verbose - Should the output be verbose?

	Using:
		Config()
			The members are: file="config.json", verbose=False
		Config("config_file.json")
			The members are: file="config_file.json", verbose=False
		Config(file="config_file.json", verbose=True)
	'''

	# -------------------------------------------------------------------------
	def __init__(self, *args, **kwargs):
		self._file = "config.json"
		if len(args) > 0:
			self._file = kwargs.pop('file', args[0])
		self._verbose = kwargs.pop('verbose', False)

	# -------------------------------------------------------------------------
	@property
	def file(self): return self._file

	# -------------------------------------------------------------------------
	@file.setter
	def file(self, filename): self._file = filename

	# -------------------------------------------------------------------------
	@property
	def verbose(self): return self._verbose

	# -------------------------------------------------------------------------
	@verbose.setter
	def verbose(self, value): self._verbose = value


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class FreshConfig(Config):
	'''
	Configuration of FRESH system.

	Properties:
		file              - Name or path of configuration-file (Config)
		verbose           - Should output verbosly? (Config)

		path_gnu          - Path of GNU toolchain location
		path_vc           - Path of MS Visual Studio (Build Tools) location
		path_ida          - Path of IDA location
		path_objects      - Path of extracted object-files (default: ./target)
		path_signatures   - Path of collected signatures (default: ./target)
		target            - Name or path of the target library (file or directory)
		processor         - The name of processor supported by IDA to process objects
		extract_objects   - Should objects be extracted or they are already extracted?
		library_type      - The type of the processed library
		addressing        - The addressing bit-length
		file_signatures   - The file of signatures collected from library (default: __function_signatures.yar) 
		collection_script - The name of script that will be executed inside IDA (default: yara_collect.py)
		temp_file         - The temporary file produced by yara-collect.py script to signal that job is done (default: object_file-collect-done.tmp).
		log_ida           - The log-file produced by IDA for every execution of collection_script (default: __ida.log)
		log_file          - The name of the log-file (default: fresh.log)
		exe_ida           - The path to executable file of IDA (idat, idat64, idat.exe, idat64.exe)
		cmd_ida           - The list of command-line argumnets for execution of IDA by subprocess.Popen() function.
		                    The target-file is not included, add it by: cmd_ida.append("path_of_target.o") 

	Constructor:
		FreshConfig(file=str(), verbose=bool(), target=str(), libtype=str(), 
					extract=bool(), addressing=int(), proc=str(), pathobj=str(), 
					pathida=str(), pathgnu=str(), pathvc=str())
	'''
	_library_vendor = {"gnu":"gnu", "msvc":"msvc", "osx":"xtools", "intel":"icc"}
	_address_len = {"byte":8, "short":16, "long":32, "int64":64}

	# -------------------------------------------------------------------------
	@staticmethod
	def library_vendor(): return FreshConfig._library_vendor

	# -------------------------------------------------------------------------
	@staticmethod
	def library_vendor_gnu(vendor): return vendor == FreshConfig._library_vendor["gnu"]

	# -------------------------------------------------------------------------
	@staticmethod
	def library_vendor_msvc(vendor): return vendor == FreshConfig._library_vendor["msvc"]

	# -------------------------------------------------------------------------
	@staticmethod
	def library_vendor_osx(vendor): return vendor == FreshConfig._library_vendor["osx"]

	# -------------------------------------------------------------------------
	@staticmethod
	def library_vendor_intel(vendor): return vendor == FreshConfig._library_vendor["intel"]

	# -------------------------------------------------------------------------
	@staticmethod
	def address_len(): return FreshConfig._address_len

	# -------------------------------------------------------------------------
	def __init__(self, *args, **kwargs):
		super(FreshConfig, self).__init__(*args, **kwargs)
		self._path_gnu = kwargs.pop('pathgnu', "/usr/bin")
		self._path_vc = kwargs.pop('pathvc', "C:/Program Files (x86)/Microsoft Visual Studio 14.0/VC/bin")
		
		# Set path of IDA installation
		self._path_ida = kwargs.pop('pathida', "C:/Program Files/IDA Pro 7.6")
		if sys.platform.startswith('darwin'):
			self._path_ida = "/Applications/IDA Pro 7.1/ida.app/Contents/MacOS"
		elif sys.platform.startswith('linux'):
			self._path_ida = os.path.expanduser("~/ida-7.0")

		self._path_objects = kwargs.pop('pathobj', "./objects")
		self._path_signatures = kwargs.pop('pathsig', "./signatures")
		self._target = kwargs.pop('target', None)
		self._processor = kwargs.pop('proc', "meta_pc")
		self._extract_obj = kwargs.pop('extract', False)
		self._library_type = FreshConfig._library_vendor[kwargs.pop('libtype', "gnu")]
		self._addressing = FreshConfig._address_len[kwargs.pop('addressing', "long")]
		self._file_signatures = "__function_signatures.yar"
		self._collection_script = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../ida_scripts/yara-collect.py")
		self._temp_file = "-collect-done.tmp"	# should be: f"{object_file}-collect-done.tmp"
		self._log_ida = "__ida.log"
		self._log_file = "__fresh.log"

		self._exe_ida = None
		self._cmd_ida = None

	# -------------------------------------------------------------------------
	def dump(self):
		address_len_keys = list(FreshConfig._address_len.keys())
		address_len_values = list(FreshConfig._address_len.values())

		lib_vendor_keys = list(FreshConfig._library_vendor.keys())
		lib_vendor_values = list(FreshConfig._library_vendor.values())

		d_fresh = {
			"file"              : self._file,
			"path_gnu"          : self._path_gnu,
			"path_vc"           : self._path_vc,
			"path_ida"          : self._path_ida,
			"path_objects"      : self._path_objects,
			"path_signatures"   : self._path_signatures,
			"target"            : self._target,
			"processor"         : self._processor,
			"extract_objects"   : self._extract_obj,
			"library_type"      : lib_vendor_keys[lib_vendor_values.index(self._library_type)],
			"addressing"        : address_len_keys[address_len_values.index(self._addressing)],
			"file_signatures"   : self._file_signatures,
			"collection_script" : self._collection_script,
			"temp_file"         : self._temp_file,
			"log_ida"           : self._log_ida,
			"log_file"          : self._log_file,
			"exe_ida"           : self._exe_ida,
			"cmd_ida"           : self._cmd_ida,
		}
		with open(self._file, 'w') as f_cfg:
			json.dump(d_fresh, f_cfg, indent=4)

	# -------------------------------------------------------------------------
	def load(self):
		if os.path.exists(self._file):
			with open(self._file) as f_cfg:
				d_fresh = json.load(f_cfg)

			self._file				= d_fresh["file"]
			self._path_gnu			= d_fresh["path_gnu"]
			self._path_vc			= d_fresh["path_vc"]
			self._path_ida			= d_fresh["path_ida"]
			self._path_objects		= d_fresh["path_objects"]
			self._path_signatures	= d_fresh["path_signatures"]
			self._target			= d_fresh["target"]
			self._processor			= d_fresh["processor"]
			self._extract_obj		= d_fresh["extract_objects"]
			self._library_type		= FreshConfig._library_vendor[d_fresh["library_type"]]
			self._addressing		= FreshConfig._address_len[d_fresh["addressing"]]
			self._file_signatures	= d_fresh["file_signatures"]
			self._collection_script	= d_fresh["collection_script"]
			self._temp_file			= d_fresh["temp_file"]
			self._log_ida			= d_fresh["log_ida"]
			self._log_file			= d_fresh["log_file"]
			self._exe_ida			= d_fresh["exe_ida"]
			self._cmd_ida			= d_fresh["cmd_ida"]
			

	# -------------------------------------------------------------------------
	def exe_ar(self): return os.path.join(self._path_gnu, "ar")

	# -------------------------------------------------------------------------
	def exe_lib(self): return os.path.join(self._path_gnu, "lib.exe")

	# -------------------------------------------------------------------------
	def exe_ida__build(self): 
		'''Set path of IDA's executable.'''
		self._exe_ida = os.path.join(self._path_ida, "idat")
		if self._addressing > 32:
			self._exe_ida += "64"
		if sys.platform.startswith('win32'):
			self._exe_ida += ".exe"
		return self._exe_ida

	# -------------------------------------------------------------------------
	@property
	def exe_ida(self): 
		'''Set path of IDA's executable.'''
		if self._exe_ida is not None:
			return self._exe_ida
		return self.exe_ida__build()

	# -------------------------------------------------------------------------
	@exe_ida.setter
	def exe_ida(self, path): 
		if path is not None: 
			self._exe_ida = path
	
	# -------------------------------------------------------------------------
	@property
	def path_gnu(self): return self._path_gnu

	# -------------------------------------------------------------------------
	@path_gnu.setter
	def path_gnu(self, path):
		if path is not None: 
			 self._path_gnu = path
	
	# -------------------------------------------------------------------------
	@property
	def path_vc(self): return self._path_vc

	# -------------------------------------------------------------------------
	@path_vc.setter
	def path_vc(self, path): 
		if path is not None: 
			self._path_vc = path
	
	# -------------------------------------------------------------------------
	@property
	def path_ida(self): return self._path_ida

	# -------------------------------------------------------------------------
	@path_ida.setter
	def path_ida(self, path): 
		if path is not None: 
			self._path_ida = path
	
	# -------------------------------------------------------------------------
	@property
	def path_objects(self): return self._path_objects

	# -------------------------------------------------------------------------
	@path_objects.setter
	def path_objects(self, path): 
		if path is not None: 
			self._path_objects = path
	
	# -------------------------------------------------------------------------
	@property
	def path_signatures(self): return self._path_signatures

	# -------------------------------------------------------------------------
	@path_signatures.setter
	def path_signatures(self, path): 
		if path is not None: 
			self._path_signatures = path
	
	# -------------------------------------------------------------------------
	@property
	def target(self): return self._target

	# -------------------------------------------------------------------------
	@target.setter
	def target(self, name): 
		if name is not None: 
			self._target = name
	
	# -------------------------------------------------------------------------
	@property
	def processor(self): return self._processor

	# -------------------------------------------------------------------------
	@processor.setter
	def processor(self, procname): 
		if procname is not None: 
			self._processor = procname
	
	# -------------------------------------------------------------------------
	@property
	def extract_objects(self): return self._extract_obj

	# -------------------------------------------------------------------------
	@extract_objects.setter
	def extract_objects(self, value): 
		if value is not None: 
			self._extract_obj = value
	
	# -------------------------------------------------------------------------
	@property
	def library_type(self): return self._library_type

	# -------------------------------------------------------------------------
	@library_type.setter
	def library_type(self, libtype): 
		if libtype is not None: 
			self._library_type = FreshConfig._library_vendor[libtype]
	
	# -------------------------------------------------------------------------
	@property
	def addressing(self): return self._addressing

	# -------------------------------------------------------------------------
	@addressing.setter
	def addressing(self, bitlen): 
		if bitlen is not None: 
			self._addressing = FreshConfig._address_len[bitlen]
	
	# -------------------------------------------------------------------------
	@property
	def file_signatures(self): return self._file_signatures

	# -------------------------------------------------------------------------
	@file_signatures.setter
	def file_signatures(self, name): 
		if name is not None: 
			self._file_signatures = name
	
	# -------------------------------------------------------------------------
	@property
	def collection_script(self): return self._collection_script

	# -------------------------------------------------------------------------
	@collection_script.setter
	def collection_script(self, name): 
		if name is not None: 
			self._collection_script = name
	
	# -------------------------------------------------------------------------
	@property
	def log_ida(self): return self._log_ida

	# -------------------------------------------------------------------------
	@log_ida.setter
	def log_ida(self, name): 
		if name is not None: 
			self._log_ida = name
	
	# -------------------------------------------------------------------------
	@property
	def log_file(self): return self._log_file

	# -------------------------------------------------------------------------
	@log_file.setter
	def log_file(self, name): 
		if name is not None: 
			self._log_file = name
	
	# -------------------------------------------------------------------------
	@property
	def temp_file(self): return self._temp_file

	# -------------------------------------------------------------------------
	@temp_file.setter
	def temp_file(self, name): 
		if name is not None: 
			self._temp_file = name
	
	# -------------------------------------------------------------------------
	@property
	def cmd_ida(self): 
		if self._cmd_ida is not None:
			return self._cmd_ida

		ida_script = os.path.abspath(self._collection_script)
		self._cmd_ida = [self.exe_ida, '-B', '-c', '-S{}'.format(ida_script)]

		if self._processor != "meta_pc":
			self._cmd_ida.append('-p{}'.format(self._processor))

		return self._cmd_ida

	# -------------------------------------------------------------------------
	@cmd_ida.setter
	def cmd_ida(self, cmd): 
		if cmd is not None: 
			self._cmd_ida = cmd
	

