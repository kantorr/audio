#! /usr/bin/env python3

'''
Simple stdio logger.

Date:
	26 April 2021
Author:
	kantorr
'''

# TODO: Make multithreaded logger, logger-thread + queue.

import sys
import os

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class LogVerbosity:
	SILENT = 0
	NORMAL = 1
	VERBOSE = 2
	DETAILED = 3

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class LogColor:
	ACCENT_STRONG = '\033[95m'		# magenta
	ACCENT_LIGHT  = '\033[94m'		# ligth magenta
	SUCCESS       = '\033[92m'		# Green
	WARNING       = '\033[93m'		# Yellow
	ERROR         = '\033[91m'		# Red
	CLEAR         = '\033[0m'		# revert to normal light gray color

	# -------------------------------------------------------------------------
	@staticmethod
	def error(msg): return LogColor.ERROR + msg + LogColor.CLEAR

	# -------------------------------------------------------------------------
	@staticmethod
	def warning(msg): return LogColor.WARNING + msg + LogColor.CLEAR

	# -------------------------------------------------------------------------
	@staticmethod
	def success(msg): return LogColor.SUCCESS + msg + LogColor.CLEAR

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_light(msg): return LogColor.ACCENT_LIGHT + msg + LogColor.CLEAR

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_strong(msg): return LogColor.ACCENT_STRONG + msg + LogColor.CLEAR

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class LogSimple:
	# -------------------------------------------------------------------------
	@staticmethod
	def error(msg): return msg

	# -------------------------------------------------------------------------
	@staticmethod
	def warning(msg): return msg

	# -------------------------------------------------------------------------
	@staticmethod
	def success(msg): return msg

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_light(msg): return msg

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_strong(msg): return msg

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Log:
	_log = LogSimple if os.name == 'nt' else LogColor

	# -------------------------------------------------------------------------
	@staticmethod
	def error(msg): return Log._log.error(msg)

	# -------------------------------------------------------------------------
	@staticmethod
	def warning(msg): return Log._log.warning(msg)

	# -------------------------------------------------------------------------
	@staticmethod
	def success(msg): return Log._log.success(msg)

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_light(msg): return Log._log.accent_light(msg)

	# -------------------------------------------------------------------------
	@staticmethod
	def accent_strong(msg): return Log._log.accent_strong(msg)

from .queue_thread import QueueThread
import time
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class LogStdIOThread(QueueThread):
	'''
	The simple logger thread.
	Prints out the colored log-message to stdio.
	'''

	# -------------------------------------------------------------------------
	def __init__(self):
		super().__init__()

	# -------------------------------------------------------------------------
	def close(self):
		# Wait for the queue to empty
		while self.pool_size() > 0:
			time.sleep(0.05)

		# Stop the thread
		self.stop()

		# Wait for the thread to complete
		self.join()

	# -------------------------------------------------------------------------
	def process_item(self, msg):
		print(msg)

	# -------------------------------------------------------------------------
	def critical(self, msg): self.add_item(f"CRITICAL: {Log._log.error(msg)}")

	# -------------------------------------------------------------------------
	def error(self, msg): self.add_item(f"ERROR: {Log._log.error(msg)}")

	# -------------------------------------------------------------------------
	def warning(self, msg): self.add_item(f"WARNING: {Log._log.warning(msg)}")

	# -------------------------------------------------------------------------
	def regular(self, msg):	self.add_item(msg)

	# -------------------------------------------------------------------------
	def success(self, msg): self.add_item(Log._log.success(msg))

	# -------------------------------------------------------------------------
	def accent_light(self, msg): self.add_item(Log._log.accent_light(msg))

	# -------------------------------------------------------------------------
	def accent_strong(self, msg): self.add_item(Log._log.accent_strong(msg))

