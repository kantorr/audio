from .logger import Log
from .logger import LogStdIOThread
from .file_system import is_exists
from .file_system import is_file
from .file_system import is_directory
from .file_system import remove_file
from .file_system import mkdir
from .file_system import File as FsFile
from .file_system import Directory as FsDirectory
from .lock_queue import LockQueue
from .queue_thread import ExtQueueThread
from .queue_thread import terminate_threads

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import os
import argparse
import subprocess
import time

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
g_args = None

g_log = LogStdIOThread()
g_convert_jobs = []

g_max_file_name = None

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Config:
	this			= "human_band"
	version			= "2.2"
	dir_flac		= "FLAC-HUMAN"
	dir_spectro		= "spectrograms"
	# sox filter: sinc tbwHP freqHP-freqLP tbwLP
	freq_band_full	   = ['-t',  '5',  '5-24k', '-t',  '8k']	# High-Pass from  5 Hz till  0 Hz; Low-Pass from 24 kHz till 32 kHz
	freq_band_moderate = ['-t', '12', '20-20k', '-t', '12k']	# High-Pass from 20 Hz till  8 Hz; Low-Pass from 20 kHz till 32 kHz
	freq_band_short	   = ['-t', '12', '24-16k', '-t', '12k']	# High-Pass from 24 Hz till 12 Hz; Low-Pass from 16 kHz till 28 kHz
	freq_rate_44	= "44100"
	freq_rate_48	= "48000"
	sox_verbose		= "-S"
	support_formats	= [".flac", ".wav"]
	description		= "Apply a sinc kaiser-windowed Band-Pass Filter in human hearing frequencies range\n" +\
		"on to files in FLAC format with sample-rate based on 48 kHz (48, 96, 192 kHz) or\n" +\
		"44.1 kHz (44.1, 88.2, 176.4 kHz)."

# -----------------------------------------------------------------------------
def log_version_heading() -> str:
	g_log.accent_light(f"{Config.this}: " + Log.warning(f"v.{Config.version}"))
	g_log.accent_light(Config.description)
	g_log.accent_light("The output directory: " + Log.warning(f"{Config.dir_flac}"))

# -----------------------------------------------------------------------------
def build_version_heading() -> str:
	s = f"{Config.this}: v.{Config.version}\n"
	s += f"{Config.description}\n"
	s += f"The output directory: {Config.dir_flac}"
	return s

# -----------------------------------------------------------------------------
def receive_arguments():
	'''Parsed arguments are accessible through the global variable: g_args.'''
	arg_parser = argparse.ArgumentParser(f"{Config.this}.py", 
		description = build_version_heading(), 
		formatter_class=argparse.RawTextHelpFormatter)

	arg_parser.add_argument("target", 
		help="The single file to convert or folder contains audio files to be converted.")

	arg_parser.add_argument("-r", "--rate", 
		help="Convert to desired sample-rate (-r 44100, --rate 48000).\nThe default rate is 48 kHz.", 
		choices=['44100', '48000'], 
		default='48000')

	arg_parser.add_argument("-b", "--bpf", 
		help="Apply Band-Pass filter with desired band of frequencies (-b 0, -b 1, --bpf 2).\nThe corresponding bands are: 0: 5Hz - 24kHz (default), 1: 20Hz - 20kHz, 2: 24Hz - 16kHz.", 
		choices=['0', '1', '2'], 
		default='0')

	arg_parser.add_argument("-v", 
		"--verbose", 
		help="Verbose output, good for debugging.", 
		action="store_true")

	global g_args
	g_args = arg_parser.parse_args()

# -----------------------------------------------------------------------------
def scale_to_sample_rate(infile: str, 
						 outfile: str, 
						 samplerate: str ='48000', 
						 downscale: bool = True, 
						 compression: int = 5) -> str:
	'''
	Scale the given audio-file to desired sample-rate (almost down-scaling).

	Arguments:
		audiofile  - A path to the audio-file for processing.
		outfile    - A path to the resulted file.
		samplerate - A desired audio sample-rate, 44100, 48000 or other (default: 48000).
		downscale  - Boolean flag indicating down-scaling (True) or up-scaling (False) of a sample-rate.

	Return:
		A path to scaled audio-file.
	'''
	sox_verbose = Config.sox_verbose
	if g_args.verbose is True:
		sox_verbose = None

	cmd_sox = ['sox']

	if sox_verbose is not None:
		cmd_sox.append(sox_verbose)

	cmd_sox.extend(['--guard', infile, '--compression', f'{compression:d}', outfile])

	if downscale:
		# Down-scaling
		cmd_sox.extend(['rate', '-v', '-I', '-b', '90', samplerate])
	else:
		# Up-scaling
		cmd_sox.extend(['rate', '-v', '-s', '-a', samplerate])

	if g_args.verbose is True:
		subprocess.run(cmd_sox)
	else:
		subprocess.run(cmd_sox, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	if not is_exists(outfile):
		msg = f"SoX failed scale to rate {samplerate}: {outfile}"
		g_log.error(msg)
		raise FileNotFoundError(msg)

	return outfile

# -----------------------------------------------------------------------------
def band_pass_filter(infile: str, outfile: str, compression: int = 5) -> str:
	'''
	Apply Band-Pass Filter of human hearing range.
	Low-Pass filter will cut at 20kHz or 16k with bandwith of 12kHz according to cmd-arguments.
	High-Pass filter will cut at 20Hz or 24Hz with bandwith of 12Hz according to cmd-arguments.

	Arguments:
		audiofile   - A path to the audio-file for processing.
		outfile     - A path to the resulted file.
		compression - The compression level for output FLAC file (default 5).

	Return:
		A path to filtered audio-file.
	'''
	sox_verbose = Config.sox_verbose
	if g_args.verbose is True:
		sox_verbose = None

	freq_bpf = Config.freq_band_full 
	if g_args.bpf == 1:
		freq_bpf = Config.freq_band_moderate
	elif g_args.bpf == 2:
		freq_bpf = Config.freq_band_short

	cmd_sox = ['sox']
	if sox_verbose is not None:
		cmd_sox.append(sox_verbose)
	cmd_sox.extend([infile, '--compression', f'{compression:d}', outfile, 'sinc'])
	cmd_sox.extend(freq_bpf)

	if g_args.verbose is True:
		subprocess.run(cmd_sox)
	else:
		subprocess.run(cmd_sox, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	if not is_exists(outfile):
		msg = f"SoX failed to filter with BPF: {outfile}"
		g_log.error(msg)
		raise FileNotFoundError(msg)

	return outfile

# -----------------------------------------------------------------------------
def spectrogram(audiofile: str, outfile: str) -> str:
	'''
	Build a spectrogram for given audio-file, the spectrogram is saved as PNG file.

	Arguments:
		audiofile - A path to the audio-file for spectrogram building.
		outfile   - A path to the spectrogram-file.

	Return:
		A path to produced spectrogram as a PNG file.
	'''
	sox_verbose = Config.sox_verbose
	if g_args.verbose is True:
		sox_verbose = None

	cmd_sox = ['sox']
	if sox_verbose is not None:
		cmd_sox.append(sox_verbose)
	cmd_sox.extend([audiofile, '-n', 'spectrogram', '-o', outfile])

	if g_args.verbose is True:
		subprocess.run(cmd_sox)
	else:
		subprocess.run(cmd_sox, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	if not is_exists(outfile):
		msg = f"SoX failed to make spectrogram: {outfile}"
		g_log.error(msg)
		raise FileNotFoundError(msg)

	return outfile

# -----------------------------------------------------------------------------
def get_sample_rate(audiofile: str) -> str:
	'''
	Determine in Hz a sample-rate of the given audio-file.

	Return:
		The string representing a sample-rate in Hz.
	'''
	cmd_soxi = ['soxi', '-r', audiofile]

	if g_args.verbose is True:
		proc_soxi = subprocess.run(cmd_soxi, stdout=subprocess.PIPE, text=True)
	else:
		proc_soxi = subprocess.run(cmd_soxi, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

	return proc_soxi.stdout[:-1]

# -----------------------------------------------------------------------------
def get_sample_precision(audiofile: str) -> str:
	'''
	Determine in bits an estimated sample precision of the given audio-file.

	Return:
		The string representing an estimated sample precision in bits.
	'''
	cmd_soxi = ['soxi', '-p', audiofile]

	if g_args.verbose is True:
		proc_soxi = subprocess.run(cmd_soxi, stdout=subprocess.PIPE, text=True)
	else:
		proc_soxi = subprocess.run(cmd_soxi, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

	return proc_soxi.stdout[:-1]

# -----------------------------------------------------------------------------
def str_hz_to_khz(hz: str) -> str:
	return str(int(int(hz) / 1000))

# -----------------------------------------------------------------------------
def str_log_file_name(name: str) -> str:
	spaces = " " * (g_max_file_name - len(name))
	return f'"{name}": {spaces}'

# -----------------------------------------------------------------------------
def convert_file(filepath: str):
	'''
	Convert supported audio file.
	Applying Band-Pass Filter of human hearing range and save with 24-bits in 48000 or 44100 sample-rate.
	Low-Pass filter will cut at 20kHz or 16k with bandwith of 12kHz.
	High-Pass filter will cut at 20Hz or 24Hz with bandwith of 12Hz.
	'''
	fs_file = FsFile(filepath)
	in_file_name = fs_file.name_whole()

	if in_file_name[0] == '.':
		return

	log_file_name = str_log_file_name(in_file_name)

	in_precision_hz = get_sample_precision(filepath)
	in_rate_hz = get_sample_rate(filepath)
	in_rate_khz = str_hz_to_khz(in_rate_hz)
	out_rate_hz = g_args.rate
	out_rate_khz = str_hz_to_khz(out_rate_hz)


	# Build the path of the output-file and spectrogram: 
	#   /foo/bar/audio-file.flac => /foo/bar/FLAC-HUMAN-48/audio-file.flac
	#   /foo/bar/audio-file.flac => /foo/bar/FLAC-HUMAN-48/spectrogram/audio-file.png
	out_dir = os.path.join(fs_file.parent(), Config.dir_flac + '-' + out_rate_khz)
	out_dir_spectro = os.path.join(out_dir, Config.dir_spectro)
	out_dir_spectro_rate = out_dir_spectro + "-" + in_rate_khz

	freq_bpf = Config.freq_band_full 
	if g_args.bpf == 1:
		freq_bpf = Config.freq_band_moderate
	elif g_args.bpf == 2:
		freq_bpf = Config.freq_band_short

	bpf_str = " ".join(freq_bpf)
	g_log.regular("==> " + Log.accent_strong(log_file_name) + Log.accent_light(f"Converting to {in_precision_hz}-{out_rate_hz} through BPF: {bpf_str}: "))

	mkdir(out_dir)
	mkdir(out_dir_spectro_rate)
		
	if g_args.verbose is True:
		g_log.regular("===> " + log_file_name + Log.accent_strong(f"Building original {in_rate_khz} kHz spectrogram"))

	png_file = os.path.join(out_dir_spectro_rate, fs_file.name_only() + '.png')
	spectrogram(filepath, png_file)

	in_file = filepath
	out_file = os.path.join(out_dir, fs_file.name_only() + '.flac')

	if int(in_rate_hz) < 96000:
		# Up-scaling the sample-rate to 96 kHz for proper down-scaling to desired sample-rate
		if g_args.verbose is True:
			g_log.regular("===> " + log_file_name + Log.accent_strong("Converting to sample-rate 96 kHz"))

		scale_to_sample_rate(in_file, out_file, samplerate='96000', downscale= False, compression=0)

		in_file = out_file

	if g_args.verbose is True:
		g_log.regular("===> " + log_file_name + Log.accent_strong(f"Applying Band-Pass Filter: {bpf_str}"))

	flac_bpf = os.path.join(out_dir, fs_file.name_only() + '-bpf.flac')
	band_pass_filter(in_file, flac_bpf, compression=0)

	if g_args.verbose is True:
		g_log.regular("===> " + log_file_name + Log.accent_strong(f"Down-scaling to sample-rate of {out_rate_khz} kHz"))

	scale_to_sample_rate(flac_bpf, out_file, samplerate=out_rate_hz)

	remove_file(flac_bpf)

	if g_args.verbose is True:
		g_log.regular("===> " + log_file_name + Log.accent_strong(f"Building {out_rate_khz} kHz spectrogram"))

	mkdir(out_dir_spectro)
	png_file = os.path.join(out_dir_spectro, fs_file.name_only() + '.png')
	spectrogram(out_file, png_file)

# -----------------------------------------------------------------------------
def convert_dir(dirname: str):
	'''Convert supported audio files in requested directory.'''

	# Multithreaded processing of collected object-files
	file_queue = LockQueue()
	global g_convert_jobs

	for i in range(8):
		job = ExtQueueThread(file_queue, convert_file)
		g_convert_jobs.append(job)

	# Start processing threads over the object-queue 
	for j in g_convert_jobs:
		j.start()

	fs_dir = FsDirectory(dirname)

	# Fill file-queue with pathes of files
	l_files = [f for f in fs_dir.files(recurcive=False, extensions=Config.support_formats)]
	global g_max_file_name
	g_max_file_name = max(len(FsFile(f).name_whole()) for f in l_files)
	g_log.regular("=> " + Log.accent_light(f"Converting {len(l_files)} files inside: ") + Log.success(fs_dir.name_whole()))

	for f in l_files:
		file_queue.put_item_blocked(f)

	# Wait for the queue to empty
	while file_queue.size() > 0:
		time.sleep(0.05)

	terminate_threads(g_convert_jobs)

# -----------------------------------------------------------------------------
def main():
	t_start = time.time()
	g_log.start()

	import traceback

	try:
		receive_arguments()
		global g_args

		log_version_heading()

		if not is_exists(g_args.target):
			raise FileNotFoundError(g_args.target)

		if is_file(g_args.target):
			convert_file(g_args.target)
		elif is_directory(g_args.target):
			convert_dir(g_args.target)

	except KeyboardInterrupt:
		g_log.warning("User interruption!\nGoodbye.")

	except FileNotFoundError as e:
		g_log.error("File not found: " + str(e))

	except SystemExit:
		terminate_threads(g_convert_jobs)
		g_log.close()
		os._exit(1)

	except Exception as e:
		g_log.error(str(e.__doc__))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		g_log.error(str(e))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_exc()
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_stack()

	finally:
		t_finish = time.time()

		t_delta = t_finish - t_start
		t_hours = int(t_delta / 3600)
		t_minutes = int((t_delta - t_hours * 3600) / 60)
		t_seconds = round((t_delta - (t_hours * 3600)) - (t_minutes * 60), 4)
		g_log.success(f"--- Finished through: {t_hours} hours, {t_minutes} minutes, {t_seconds} seconds")

	terminate_threads(g_convert_jobs)
	g_log.close()
	os._exit(0)
