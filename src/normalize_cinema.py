from .logger import Log
from .logger import LogStdIOThread
from .file_system import is_exists
from .file_system import is_file
from .file_system import is_directory
from .file_system import mkdir
from .file_system import File as FsFile
from .file_system import Directory as FsDirectory
from .lock_queue import LockQueue
from .queue_thread import ExtQueueThread
from .queue_thread import terminate_threads
from .timer import Timer
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import os
import platform
import argparse
import subprocess
import time

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
g_args = None
g_this_script = "normalize_cinema"
g_output_dir = "NORMALIZED_FOR_CINEMA"

g_log = LogStdIOThread()
g_normalize_jobs = []

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Config:

	# -------------------------------------------------------------------------
	@staticmethod
	def is_cpu_apple_m1() -> bool:
		return platform.system() == "Darwin" and platform.machine() == "arm64"

	# -------------------------------------------------------------------------
	@staticmethod
	def cmd_ffmpeg_normalize(in_file: str, out_file: str, lossless: bool) -> list:
		# Input-file
		cmd = ['ffmpeg-normalize', f'"{in_file}"']
		
		# Normalization type
		cmd.extend(["-nt", "ebu", "-tp", "+0.0", "--dual-mono"])

		# Progress display
		cmd.extend(["-p", "-pr"])

		# Audio-codec
		cmd.extend(["-ar", "48000"])

		if not lossless:
			cmd.extend(["-b:a", "529k", "-c:a"])
			if Config.is_cpu_apple_m1():
				cmd.extend(["aac_at"])
			else:
				cmd.extend(["aac"])
		else:
			cmd.extend(["-c:a", "flac"])

		# Output-file
		cmd.extend(["-f", "-o", f'"{out_file}"'])
		return cmd

	# -------------------------------------------------------------------------
	@staticmethod
	def supported_file_types() -> list:
		return [".flac", ".mp3", ".ogg", ".vorbis", ".wav"]

# -----------------------------------------------------------------------------
def receive_arguments():
	'''Parsed arguments are accessible through the global variable: g_args.'''
	s = f"{g_this_script}: v.2.0\n"
	s += "Normalize audio file for comfort cinema experience.\n"
	s += f'Normalized files will be placed in "{g_output_dir}" folder inside the required one\n'
	s += "Normalization will be taken by ffmpeg-normalize utility\n"
	s += "The EBU R128 algorithm will be applied with following quality:\n"
	s += "  Lossed: Maximal peak level: +0.0 dB, Encoder: AAC, Bitrate: 529k VBR, Sample Rate: 48 kHz\n"
	s += "Lossless: Maximal peak level: +0.0 dB, Encoder: FLAC, Bitrate: as sourced, Sample Rate: as sourced"
	arg_parser = argparse.ArgumentParser(f"{g_this_script}.py", 
		description = s, 
		formatter_class = argparse.RawTextHelpFormatter)
	
	arg_parser.add_argument("target", 
		help = "The desired directory contains audio files or single file.")
	
	arg_parser.add_argument("-l", "--lossless", 
		help = "Lossless output, high quality audio output in FLAC format.", 
		action = "store_true")

	global g_args
	g_args = arg_parser.parse_args()

# -----------------------------------------------------------------------------
def file_normalize(filename):
	'''Normalize a single file.'''
	fs_file = FsFile(filename)
	folder = fs_file.parent()
	
	out_ext = "m4a"
	if g_args.lossless is True:
		out_ext = "flac"
	
	out_file = os.path.join(folder, g_output_dir, f"{fs_file.name_only()}.norm.{out_ext}")

	mkdir(os.path.join(folder, g_output_dir))

	g_log.regular("==> " + Log.accent_light("Normalizing: ") + Log.success(fs_file.name_whole()))

	cmd = Config.cmd_ffmpeg_normalize(filename, out_file, g_args.lossless)

	# subprocess.Popen(" ".join(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
	proc = subprocess.Popen(" ".join(cmd), shell=True)
	proc.wait()

# -----------------------------------------------------------------------------
def dir_normalize(dirname):
	'''Normalize supported audio files in requested directory.'''

	# Multithreaded processing of collected object-files
	file_queue = LockQueue()
	global g_normalize_jobs

	for i in range(8):
		job = ExtQueueThread(file_queue, file_normalize)
		g_normalize_jobs.append(job)

	# Start processing threads over the object-queue 
	for j in g_normalize_jobs:
		j.start()

	fs_dir = FsDirectory(dirname)

	# Fill file-queue with pathes of files
	l_files = [f for f in fs_dir.files(recurcive=False, extensions=Config.supported_file_types())]
	g_log.regular("=> " + Log.accent_light(f"Normalizing {len(l_files)} files inside: ") + Log.success(fs_dir.name_whole()))

	for f in l_files:
		file_queue.put_item_blocked(f)

	# Wait for the queue to empty
	while file_queue.size() > 0:
		time.sleep(0.05)

	terminate_threads(g_normalize_jobs)

# -----------------------------------------------------------------------------
def main():
	normalize_timer = Timer(start=True)
	g_log.start()

	import traceback

	try:
		receive_arguments()
		global g_args

		if not is_exists(g_args.target):
			raise FileNotFoundError(g_args.target)

		if is_file(g_args.target):
			file_normalize(g_args.target)
		elif is_directory(g_args.target):
			dir_normalize(g_args.target)

	except KeyboardInterrupt:
		g_log.warning("User interruption!\nGoodbye.")

	except FileNotFoundError as e:
		g_log.error("File not found: " + str(e))

	except SystemExit:
		terminate_threads(g_normalize_jobs)
		g_log.close()
		os._exit(1)

	except Exception as e:
		g_log.error(str(e.__doc__))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		g_log.error(str(e))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_exc()
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_stack()

	finally:
		normalize_timer.check_point()
		(h, m, s) = normalize_timer.diff()
		g_log.success(f"--- Finished through: {h} hours, {m} minutes, {s} seconds")

	terminate_threads(g_normalize_jobs)
	g_log.close()
