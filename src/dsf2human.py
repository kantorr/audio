from .logger import Log
from .logger import LogStdIOThread
from .file_system import is_exists
from .file_system import is_file
from .file_system import is_directory
from .file_system import remove_file
from .file_system import mkdir
from .file_system import File as FsFile
from .file_system import Directory as FsDirectory
from .lock_queue import LockQueue
from .queue_thread import ExtQueueThread
from .queue_thread import terminate_threads

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import os
import argparse
import subprocess
import time
# from ffmpeg import FFmpeg
import tempfile

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
g_args = None

g_log = LogStdIOThread()
g_convert_jobs = []

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Config:
	this			= "dsf2human"
	version			= "2.4"
	dir_flac		= "FLAC-HUMAN"
	dir_spectro		= "spectrograms"
	# sox filter: sinc tbwHP freqHP-freqLP tbwLP
	freq_band_full	   = ['-t',  '5',  '5-24k', '-t',  '8k']	# High-Pass from  5 Hz till  0 Hz; Low-Pass from 24 kHz till 32 kHz
	freq_band_moderate = ['-t', '12', '20-20k', '-t', '12k']	# High-Pass from 20 Hz till  8 Hz; Low-Pass from 20 kHz till 32 kHz
	freq_band_short	   = ['-t', '12', '24-16k', '-t', '12k']	# High-Pass from 24 Hz till 12 Hz; Low-Pass from 16 kHz till 28 kHz
	freq_rate_44	= "44100"
	freq_rate_48	= "48000"
	ffmpeg_quite	= ["-v", "quiet"]
	ffmpeg_stat		= "volumedetect"			# Audio-filter to gather statistic over an audio-stream
	# Audio-filter to down-mix an audio-stream from 5.1 to stereo
	ffmpeg_downmix_6 = "pan=stereo|FL=0.5*FC+0.707*FL+0.707*BL+0.5*LFE|FR=0.5*FC+0.707*FR+0.707*BR+0.5*LFE"
	ffmpeg_downmix_8 = "pan=stereo|FL=0.5*FC+0.707*FL+0.5*SL+0.707*BL+0.5*LFE|FR=0.5*FC+0.707*FR+0.5*SR+0.707*BR+0.5*LFE"
	sox_verbose		= "-S"
	support_formats	= [".dsf", ".dff", ".dts", ".wav"]
	description		= "Convert from DSF (DSD) to 24-bit FLAC format in 48 or 44.1 kHz sample-rate.\n" + \
		"The Band-Pass filter of human hearing frequencies range is apllyied."

# -----------------------------------------------------------------------------
def log_version_heading() -> str:
	g_log.accent_light(f"{Config.this}: " + Log.warning(f"v.{Config.version}"))
	g_log.accent_light(Config.description)
	g_log.accent_light("The output directory: " + Log.warning(f"{Config.dir_flac}"))

# -----------------------------------------------------------------------------
def build_version_heading() -> str:
	s = f"{Config.this}: v.{Config.version}\n"
	s += f"{Config.description}\n"
	s += f"The output directory: {Config.dir_flac}"
	return s

# -----------------------------------------------------------------------------
def receive_arguments():
	'''Parsed arguments are accessible through the global variable: g_args.'''
	arg_parser = argparse.ArgumentParser("dsf2human.py", 
		description = build_version_heading(), 
		formatter_class=argparse.RawTextHelpFormatter)

	arg_parser.add_argument("target", 
		help="The single file to convert or folder contains audio files to be converted.")

	s =  "Downmix surround multi-track to stereo.\n"
	s += "Combining 5.1 multi-track as: 0.5*Center + 0.707*Front + 0.707*Back + 0.5*LFE\n"
	s += "Combining 7.1 multi-track as: 0.5*Center + 0.707*Front + 0.5*Side + 0.707*Back + 0.5*LFE\n"

	arg_parser.add_argument("-m", "--multi", 
		help=s,
		choices=['6', '8'])

	arg_parser.add_argument("-r", "--rate", 
		help="Convert to desired rate_value (-r 44100, --rate 48000).\nThe default rate is 48 kHz.", 
		choices=['44100', '48000'], 
		default='48000')

	arg_parser.add_argument("-b", "--bpf", 
		help="Apply Band-Pass filter with desired band of frequencies (-b 0, -b 1, --bpf 2).\nThe corresponding bands are: 0: 5Hz - 24kHz (default), 1: 20Hz - 20kHz, 2: 24Hz - 16kHz.", 
		choices=['0', '1', '2'], 
		default='0')

	arg_parser.add_argument("-v", 
		"--verbose", 
		help="Verbose output, good for debugging.", 
		action="store_true")

	global g_args
	g_args = arg_parser.parse_args()

# -----------------------------------------------------------------------------
def scale_to_sample_rate(infile, outfile, samplerate='48000'):
	'''
	Scale the given audio-file to desired sample-rate (almost down-scaling).

	Arguments:
		audiofile  - A path to the audio-file for processing.
		outfile    - A path to the resulted file.
		samplerate - A desired audio sample-rate, 44100, 48000 or other (default: 48000).

	Return:
		A path to scaled audio-file.
	'''
	sox_verbose = Config.sox_verbose
	if g_args.verbose is True:
		sox_verbose = None

	cmd_sox = ['sox']
	if sox_verbose is not None:
		cmd_sox.append(sox_verbose)
	cmd_sox.extend([infile, '--compression', '5', outfile, 'rate', '-v', '-I', '-b', '90', samplerate])

	if g_args.verbose is True:
		g_log.regular("===> " + Log.warning(" ".join(cmd_sox)))
		subprocess.run(cmd_sox)
	else:
		subprocess.run(cmd_sox, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	if not is_exists(outfile):
		msg = f"SoX failed scale to rate {samplerate}: {outfile}"
		g_log.error(msg)
		raise FileNotFoundError(msg)

	return outfile


# -----------------------------------------------------------------------------
def band_pass_filter(infile, outfile, compression=5):
	'''
	Apply Band-Pass Filter of human hearing range.
	Low-Pass filter will cut at 20kHz or 16k with bandwith of 12kHz according to cmd-arguments.
	High-Pass filter will cut at 20Hz or 24Hz with bandwith of 12Hz according to cmd-arguments.

	Arguments:
		audiofile   - A path to the audio-file for processing.
		outfile     - A path to the resulted file.
		compression - The compression level for output FLAC file (default 5).

	Return:
		A path to filtered audio-file.
	'''
	sox_verbose = Config.sox_verbose
	if g_args.verbose is True:
		sox_verbose = None

	freq_bpf = Config.freq_band_full 
	if g_args.bpf == 1:
		freq_bpf = Config.freq_band_moderate
	elif g_args.bpf == 2:
		freq_bpf = Config.freq_band_short

	cmd_sox = ['sox']
	if sox_verbose is not None:
		cmd_sox.append(sox_verbose)
	cmd_sox.extend([infile, '--compression', f'{compression:d}', outfile, 'sinc'])
	cmd_sox.extend(freq_bpf)

	if g_args.verbose is True:
		g_log.regular("===> " + Log.warning(" ".join(cmd_sox)))
		subprocess.run(cmd_sox)
	else:
		subprocess.run(cmd_sox, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	if not is_exists(outfile):
		msg = f"SoX failed to filter with BPF: {outfile}"
		g_log.error(msg)
		raise FileNotFoundError(msg)

	return outfile

# -----------------------------------------------------------------------------
def spectrogram(audiofile, outfile):
	'''
	Build a spectrogram for given audio-file, the spectrogram is saved as PNG file.

	Arguments:
		audiofile - A path to the audio-file for spectrogram building.
		outfile   - A path to the spectrogram-file.

	Return:
		A path to produced spectrogram as a PNG file.
	'''
	sox_verbose = Config.sox_verbose
	if g_args.verbose is True:
		sox_verbose = None

	cmd_sox = ['sox']
	if sox_verbose is not None:
		cmd_sox.append(sox_verbose)
	cmd_sox.extend([audiofile, '-n', 'spectrogram', '-o', outfile])

	if g_args.verbose is True:
		g_log.regular("===> " + Log.warning(" ".join(cmd_sox)))
		subprocess.run(cmd_sox)
	else:
		subprocess.run(cmd_sox, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	if not is_exists(outfile):
		msg = f"SoX failed to make spectrogram: {outfile}"
		g_log.error(msg)
		raise FileNotFoundError(msg)

	return outfile

# -----------------------------------------------------------------------------
def convert_raw_to_flac_96k(rawfile, outdir):
	'''
	Convert given RAW audio-file of supported format to FLAC with sample-rate of 96 kHz.
	
	Arguments:
		rawfile - A path to the raw-file in supported format {see Config.support_formats}.
		outdir  - The output directory to store converted FLAC file.

	Return:
		A path to converted FLAC file.
	'''
	pass
	fs_raw = FsFile(rawfile)
	tmp_wav = os.path.join(outdir, fs_raw.name_only() + '.wav')
	out_flac = os.path.join(outdir, fs_raw.name_only() + '.flac')

	ffmpeg_audio_filter = Config.ffmpeg_stat
	if g_args.multi == '6':
		ffmpeg_audio_filter = Config.ffmpeg_downmix_6
	elif g_args.multi == '8':
		ffmpeg_audio_filter = Config.ffmpeg_downmix_8

	ffmpeg_quite = Config.ffmpeg_quite
	sox_verbose = Config.sox_verbose
	if g_args.verbose is True:
		ffmpeg_quite = []
		sox_verbose = None

	if g_args.verbose is True:
		g_log.regular("===> " + Log.accent_strong(f"ffmpeg audio-filter: {ffmpeg_audio_filter}"))

	# Build ffmpeg command-line
	cmd_ffmpeg = f'ffmpeg -y -i "{rawfile}" '
	if len(ffmpeg_quite) > 0:
		cmd_ffmpeg += " ".join(ffmpeg_quite)
	cmd_ffmpeg += f' -af "{ffmpeg_audio_filter}" -f wav -acodec pcm_s24le -ar 96000 "{tmp_wav}"'

	# Call ffmpeg
	if g_args.verbose is True:
		g_log.regular("===> " + Log.warning(cmd_ffmpeg))
	subprocess.run(cmd_ffmpeg, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

	if not is_exists(tmp_wav):
		msg = f"ffmpeg unable to convert: {rawfile}"
		g_log.error(msg)
		raise FileNotFoundError(msg)

	# Build SoX command-line
	cmd_sox = ['sox']
	if sox_verbose is not None:
		cmd_sox.append(sox_verbose)
	cmd_sox.extend(['-t', 'wav', tmp_wav, '--compression', '0', out_flac])

	# Call SoX
	if g_args.verbose is True:
		g_log.regular("===> " + Log.warning(" ".join(cmd_sox)))
		proc_sox = subprocess.run(cmd_sox)
	else:
		proc_sox = subprocess.run(cmd_sox, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	
	# proc_sox.wait()

	remove_file(tmp_wav)

	if not is_exists(out_flac):
		msg = f"SoX unable convert to: {out_flac}"
		g_log.error(msg)
		raise FileNotFoundError(msg)

	return out_flac

# -----------------------------------------------------------------------------
def convert_file(filename):
	'''
	Convert supported audio file.
	Applying Band-Pass Filter of human hearing range and save with 24-bits in 48000 or 44100 sample-rate.
	Low-Pass filter will cut at 20kHz or 16k with bandwith of 12kHz.
	High-Pass filter will cut at 20Hz or 24Hz with bandwith of 12Hz.
	'''
	fs_file = FsFile(filename)

	freq_rate = g_args.rate

	# Build the path of the output-file and spectrogram: 
	#   /foo/bar/audio-file.dsf => /foo/bar/FLAC-HUMAN-48/audio-file.flac
	#   /foo/bar/audio-file.dsf => /foo/bar/FLAC-HUMAN-48/spectrogram-96/audio-file.png
	#   /foo/bar/audio-file.dsf => /foo/bar/FLAC-HUMAN-48/spectrogram/audio-file.png
	out_dir = os.path.join(fs_file.parent(), Config.dir_flac + '-' + freq_rate[:2])
	out_dir_spectro = os.path.join(out_dir, Config.dir_spectro)
	out_dir_spectro_96 = out_dir_spectro + '-96'

	freq_bpf = Config.freq_band_full 
	if g_args.bpf == 1:
		freq_bpf = Config.freq_band_moderate
	elif g_args.bpf == 2:
		freq_bpf = Config.freq_band_short

	bpf_str = " ".join(freq_bpf)
	g_log.regular("==> " + Log.accent_light(f"Converting to 24-{freq_rate} through BPF: {bpf_str}: ") + Log.accent_strong(f'"{fs_file.name_whole()}"'))

	mkdir(out_dir)

	flac_file = convert_raw_to_flac_96k(filename, out_dir)
		
	if g_args.verbose is True:
		g_log.regular("===> " + Log.accent_strong("Building intermediate 96 kHz spectrogram"))

	os.makedirs(out_dir_spectro_96, exist_ok=True)
	fs_flac = FsFile(flac_file)
	png_file = os.path.join(out_dir_spectro_96, fs_flac.name_only() + '.png')
	spectrogram(flac_file, png_file)

	if g_args.verbose is True:
		g_log.regular("===> " + Log.accent_strong(f"Applying Band-Pass Filter: {bpf_str}"))

	flac_bpf = os.path.join(out_dir, fs_flac.name_only() + '-bpf.flac')
	band_pass_filter(flac_file, flac_bpf, 0)

	if g_args.verbose is True:
		g_log.regular("===> " + Log.accent_strong(f"Down-scaling to sample-rate of {g_args.rate} Hz"))

	scale_to_sample_rate(flac_bpf, flac_file, g_args.rate)

	remove_file(flac_bpf)

	if g_args.verbose is True:
		g_log.regular("===> " + Log.accent_strong(f"Building {g_args.rate} Hz spectrogram"))

	os.makedirs(out_dir_spectro, exist_ok=True)
	png_file = os.path.join(out_dir_spectro, fs_flac.name_only() + '.png')
	spectrogram(flac_file, png_file)

# -----------------------------------------------------------------------------
def convert_dir(dirname):
	'''Convert supported audio files in requested directory.'''

	fs_dir = FsDirectory(dirname)

	# Fill file-queue with pathes of files
	l_files = [f for f in fs_dir.files(recurcive=False, extensions=Config.support_formats)]
	g_log.regular("=> " + Log.accent_light(f"Converting {len(l_files)} files inside: ") + Log.success(fs_dir.name_whole()))

 	# Multithreaded processing of collected object-files
	file_queue = LockQueue()
	global g_convert_jobs

	for i in range(8):
		job = ExtQueueThread(file_queue, convert_file)
		g_convert_jobs.append(job)

	# Start processing threads over the object-queue 
	for j in g_convert_jobs:
		j.start()


	for f in l_files:
		file_queue.put_item_blocked(f)

	# Wait for the queue to empty
	while file_queue.size() > 0:
		time.sleep(0.05)

	terminate_threads(g_convert_jobs)

# -----------------------------------------------------------------------------
def main():
	t_start = time.time()
	g_log.start()

	import traceback

	try:
		receive_arguments()
		global g_args

		log_version_heading()

		if not is_exists(g_args.target):
			raise FileNotFoundError(g_args.target)

		if is_file(g_args.target):
			convert_file(g_args.target)
		elif is_directory(g_args.target):
			convert_dir(g_args.target)

	except KeyboardInterrupt:
		g_log.warning("User interruption!\nGoodbye.")

	except FileNotFoundError as e:
		g_log.error("File not found: " + str(e))

	except SystemExit:
		terminate_threads(g_convert_jobs)
		g_log.close()
		os._exit(1)

	except Exception as e:
		g_log.error(str(e.__doc__))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		g_log.error(str(e))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_exc()
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_stack()

	finally:
		t_finish = time.time()

		t_delta = t_finish - t_start
		t_hours = int(t_delta / 3600)
		t_minutes = int((t_delta - t_hours * 3600) / 60)
		t_seconds = round((t_delta - (t_hours * 3600)) - (t_minutes * 60), 4)
		g_log.success(f"--- Finished through: {t_hours} hours, {t_minutes} minutes, {t_seconds} seconds")

	terminate_threads(g_convert_jobs)
	g_log.close()
	os._exit(0)
