#! /usr/bin/env python3

'''
Basic Queue with locker.

Date:
	05 May 2021
Author:
	kantorr
'''

import threading
import queue
import time

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class LockQueue(object):
	'''Basic Queue with locker.'''

	# -------------------------------------------------------------------------
	def __init__(self, n=0):
		super().__init__()

		self._capacity = n
		self._pool = queue.Queue(n)
		self._pool_lock = threading.Lock()

	# -------------------------------------------------------------------------
	def get_item_blocked(self):
		# print(f"LockQueue.get_item_blocked: ...")
		self.lock()
		x = self._pool.get()
		# print(f"{x}")
		self.unlock()
		return x

	# -------------------------------------------------------------------------
	def get_item_non_blocked(self):
		# print(f"LockQueue.get_item_non_blocked: ...")
		x = self._pool.get()
		# print(f"{x}")
		return x

	# -------------------------------------------------------------------------
	def put_item_blocked(self, x): 
		# print(f"LockQueue.put_item_blocked: {x}")
		if self._capacity <= self.size():
			time.sleep(0.05)
		self.lock()
		self._pool.put(x)
		self.unlock()

	# -------------------------------------------------------------------------
	def put_item_non_blocked(self, x): 
		# print(f"LockQueue.put_item_non_blocked: {x}")
		if self._capacity <= self.size():
			time.sleep(0.05)
		self._pool.put(x)

	# -------------------------------------------------------------------------
	def empty(self): 
		return self._pool.qsize() == 0

	# -------------------------------------------------------------------------
	def size(self): 
		return self._pool.qsize()

	# -------------------------------------------------------------------------
	def lock(self): 
		self._pool_lock.acquire()		

	# -------------------------------------------------------------------------
	def unlock(self): 
		self._pool_lock.release()

	# -------------------------------------------------------------------------
	def is_locked(self): 
		return self._pool_lock.locked()		
