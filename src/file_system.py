# file_system.py

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import os
import errno
import sys
import stat
import pathlib
from shutil import copyfile

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if os.name == 'nt':
	# needs win32all to work on Windows
	import win32con, win32file, pywintypes
	LOCK_EX = win32con.LOCKFILE_EXCLUSIVE_LOCK
	LOCK_SH = 0 # the default
	LOCK_NB = win32con.LOCKFILE_FAIL_IMMEDIATELY
	__overlapped = pywintypes.OVERLAPPED()

	#--------------------------------------------------------------------------
	def lock(file, flags):
		hfile = win32file._get_osfhandle(file.fileno())
		win32file.LockFileEx(hfile, flags, 0, 0xffff0000, __overlapped)

	#--------------------------------------------------------------------------
	def unlock(file):
		hfile = win32file._get_osfhandle(file.fileno())
		win32file.UnlockFileEx(hfile, 0, 0xffff0000, __overlapped)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
elif os.name == 'posix':
	import fcntl
	from fcntl import LOCK_EX, LOCK_SH, LOCK_NB

	#--------------------------------------------------------------------------
	def lock(file, flags):
		fcntl.flock(file.fileno(), flags)

	#--------------------------------------------------------------------------
	def unlock(file):
		fcntl.flock(file.fileno(), fcntl.LOCK_UN)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
else:
	raise RuntimeError("Collection is possible only under NT or POSIX platforms!")

# -----------------------------------------------------------------------------
def is_exists(abspath):
	return os.path.exists(abspath)

# -----------------------------------------------------------------------------
def is_file(abspath):
	return stat.S_ISREG(os.stat(abspath).st_mode)

# -----------------------------------------------------------------------------
def is_directory(abspath):
	return stat.S_ISDIR(os.stat(abspath).st_mode)

# -----------------------------------------------------------------------------
def mkdir(abspath):
	'''Make dir like os.mkdir() without exception on existing directory.'''
	try:
		os.mkdir(abspath)
	except FileExistsError as e:
		pass

# -----------------------------------------------------------------------------
def remove_file(path):
	try:
		os.remove(path)
	except FileNotFoundError:
		pass

# -----------------------------------------------------------------------------
def move_file(dstpath, srcpath):
	copyfile(srcpath, dstpath)
	os.remove(srcpath)

# -----------------------------------------------------------------------------
def copy_file(dstpath, srcpath):
	# print("file_system.copy_file: dst: {} src: {}".format(dstpath, srcpath))
	copyfile(srcpath, dstpath)

	# bflag = 0
	# if sys.platform == 'win32':
	# 	bflag = os.O_BINARY

	# fd_src = os.open(srcpath, os.O_RDONLY | bflag)
	# if fd_src:
	# 	size = os.stat(srcpath).st_size
	# 	fd_dst = os.open(dstpath, os.O_CREAT | os.O_WRONLY | bflag)
	# 	if fd_dst:
	# 		try:
	# 			os.copy_file_range(fd_src, fd_dst, size)
	# 		except OSError as e:
	# 			if e.errno == errno.EXDEV:
	# 				os.sendfile(fd_dst, fd_src, 0, size)
	# 			pass
	# 		except Exception as e:
	# 			os.sendfile(fd_dst, fd_src, 0, size)
	# 			pass
	# 		finally:
	# 			os.close(fd_dst)
	# 			os.close(fd_src)


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class File:
	'''
	A generic class representing the system's file.
	'''
	# -------------------------------------------------------------------------
	def __init__(self, *args, **kwargs):
		name = kwargs.pop('name', None)
		if name is None and len(args[0]) > 0:
			name = args[0]
		self._mode = kwargs.pop('mode', 'rb')
		self._path = pathlib.Path(os.path.abspath(name))

	# -------------------------------------------------------------------------
	def __str__(self):
		return self.abspath()

	# -------------------------------------------------------------------------
	def size(self):
		''''Returns the size of file in bytes.'''
		return self._path.stat().st_size

	# -------------------------------------------------------------------------
	def abspath(self):
		'''Returns a string representing the absolute path of the file ("/foo/bar/setup.py").'''
		return str(self._path.resolve())

	# -------------------------------------------------------------------------
	def parent(self):
		'''Returns the logical parent of the file ("/foo/bar" in "/foo/bar/setup.py").'''
		return str(self._path.parent.resolve())

	# -------------------------------------------------------------------------
	def replace_path(self, oldpath, newpath):
		'''
		Replace old_path by a new_path in the absolute path of the file.
		("/foo => /new: /foo/bar/setup.py" become "/new/bar/setup.py").
		'''
		terminators = ('\\', '/')
		if oldpath[-1] in terminators and newpath[-1] not in terminators:
			newpath += oldpath[-1]

		s = str(self._path).replace(oldpath, newpath)
		self._path = pathlib.Path(s)

		return str(self._path)

	# -------------------------------------------------------------------------
	def name_whole(self):
		'''A string representing the name of the file, with extension (the final path component: "setup.py").'''
		return str(self._path.name)

	# -------------------------------------------------------------------------
	def name_only(self):
		'''A string representing the name of the file, without extension 
		   (the final path component, without suffix: "setup" in "setup.py").'''
		return str(self._path.stem)

	# -------------------------------------------------------------------------
	def extension(self):
		'''A string representing the extension of the file, if any ("py" in "setup.py").'''
		if self._path.suffixes is not None and len(self._path.suffixes) > 0:
			return str(self._path.suffixes[-1])
		return ""

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Directory(File):
	'''
	A generic class representing the system's directory.
	'''
	# -------------------------------------------------------------------------
	def __init__(self, *args, **kwargs):
		super(Directory, self).__init__(*args, **kwargs)

	# -------------------------------------------------------------------------
	def files(self, recurcive=False, names=None, extensions=None):
		'''
		Generate names of files in the directory.
		
		Arguments:
			recurcive - Boolean, make recurcive list of files from all sub-directories.
			ext       - List of file-extensions to be searched [".foo", ".bar"].

		Return:
			The list of strings representing absolute path of found files.
		'''
		file_db = {}	# {n:path}
		dir_db = {}		# {n:path}
		n_files = 0
		n_dirs = 0
		for f_name in os.listdir(self._path):
			f_path = self._path / f_name
			path = str(f_path.resolve())
			if is_file(path):
				fs_file = File(path)
				if extensions is None and names is None:
					file_db[n_files] = path
					n_files += 1
				else:
					if extensions is not None and fs_file.extension() in extensions:
						file_db[n_files] = path
					if names is not None:
						if fs_file.name_only() in names or fs_file.name_whole() in names:
							file_db[n_files] = path	# Nevermind if the path was added before because same extension, just overwrite it.
					n_files += 1
			elif recurcive and is_directory(path):
				dir_db[n_dirs] = path
				n_dirs += 1

		for k in file_db:
			yield file_db[k]

		if recurcive:
			for k in dir_db: 
				for f in Directory(dir_db[k]).files(recurcive=recurcive, extensions=extensions):
					yield f

	# -------------------------------------------------------------------------
	def remove_files(self, recurcive=False, names=None, extensions=None):
		'''
		Remove files from directory and it's sub-directories.
		'''
		for f in self.files(recurcive=recurcive, names=names, extensions=extensions):
			remove_file(f)

	# -------------------------------------------------------------------------
	def process_files(self, fncallback, recurcive=False, names=None, extensions=None):
		'''
		Process files of directory and sub-directories by given callback.		
		'''
		for f in self.files(recurcive=recurcive, names=names, extensions=extensions):
			fncallback(f)

	# -------------------------------------------------------------------------
	def directories(self):
		'''Generate names of directories in the directory.'''
		dir_db = {}		# {n:path}
		n_dirs = 0
		for f_name in os.listdir(self._path):
			f_path = self._path / f_name
			path = str(f_path.resolve())
			if is_directory(path):
				dir_db[n_dirs] = path
				n_dirs += 1

		for k in dir_db:
			yield dir_db[k]

	# -------------------------------------------------------------------------
	def size(self):
		'''Calculate the size of the directory, the size of all entities in the directory.'''
		whole_size = 0
		for f_name in os.listdir(self._path):
			f_path = self._path / f_name
			path = str(f_path.resolve())
			if is_directory(path):
				whole_size += Directory(path).size()
			if is_file(path):
				whole_size += os.stat(path).st_size
		return whole_size

	# -------------------------------------------------------------------------
	def walk(self):
		'''
		Generate the file names in a directory tree walking the tree top-down.
		For each directory in the tree rooted at directory top (including top itself),
		it yields a 3-tuple: (dirpath, dirnames, filenames).
		For more information refer to Python documentation about: os.walk() function.
		'''
		return os.walk(str(self._path))

	# -------------------------------------------------------------------------
	def is_empty(self):
		'''Is the directory empty of files or other directories?'''
		return len(os.listdir(str(self._path))) == 0

	# -------------------------------------------------------------------------
	def is_empty_of_files(self):
		'''Is the directory empty of files?'''
		return len([f for f in self.files()]) == 0

	# -------------------------------------------------------------------------
	def is_empty_of_directories(self):
		'''Is the directory empty of directories?'''
		return len([f for f in self.directories()]) == 0
