#! /usr/bin/env python3

'''
Basic thread with queue.

Date:
	05 May 2021
Author:
	kantorr
'''

import threading
import queue
import time

from .lock_queue import LockQueue

# -----------------------------------------------------------------------------
def terminate_threads(lthreads):
	'''Stop job-threads.'''
	# Stop all threads in the given list
	for th in lthreads:
		if th.is_alive():
			th.stop()

	# Wait for thread's termination
	for th in lthreads:
		if th.is_alive():
			th.join()

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class QueueThread(threading.Thread):

	# -------------------------------------------------------------------------
	def __init__(self, n=0, timeout=0.1):
		super().__init__()

		self._pool = LockQueue(n)
		self._stop_pulling = False
		self._pool_lock = threading.Lock()
		self._timeout = timeout

	# -------------------------------------------------------------------------
	def stop(self): 
		while self._pool.size() > 0:
			time.sleep(0.05)

		self._stop_pulling = True

	# -------------------------------------------------------------------------
	def run(self):
		while not self._stop_pulling:
			self._pool.lock()
			# print(f"QueueThread.run: pool size: {self._pool.size()}")
			if self._pool.size() > 0:
				item = self._pool.get_item_non_blocked()
				self._pool.unlock()		
				# Process item: call processing method in a child class
				self.process_item(item)
			else:
				self._pool.unlock()
				time.sleep(self._timeout)

	# -------------------------------------------------------------------------
	def process_item(self, x):
		raise Exception("Unimplemented: QueueThread.process_item(): Implement it in the child class!")

	# -------------------------------------------------------------------------
	def add_item(self, x): 
		self._pool.put_item_blocked(x)

	# -------------------------------------------------------------------------
	def pool_size(self): 
		return self._pool.size()

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class ExtQueueThread(threading.Thread):

	# -------------------------------------------------------------------------
	def __init__(self, lockqueue: LockQueue, fncallback, timeout=0.1):
		super().__init__()

		self._pool = lockqueue
		self._fn_process_item = fncallback
		self._timeout = timeout
		self._stop_pulling = False

	# -------------------------------------------------------------------------
	def stop(self): 
		# while self._pool.size() > 0:
		# 	time.sleep(0.05)

		self._stop_pulling = True

	# -------------------------------------------------------------------------
	def run(self):
		while not self._stop_pulling:
			self._pool.lock()
			if self._pool.size() > 0:
				item = self._pool.get_item_non_blocked()
				self._pool.unlock()		
				# Process item: call processing method in a child class
				self._fn_process_item(item)
			else:
				self._pool.unlock()
				time.sleep(self._timeout)

	# -------------------------------------------------------------------------
	def close(self):
		# Stop the thread
		self.stop()
		# Wait for the thread to complete
		self.join()

	# -------------------------------------------------------------------------
	def pool_size(self): 
		return self._pool.size()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
g_process_jobs = []

# -----------------------------------------------------------------------------
def process_terminate():
	global g_process_jobs
	terminate_threads(g_process_jobs)
	g_process_jobs = []

# -----------------------------------------------------------------------------
def process_multiple(items: list, func):
	'''Multithreaded processing of items in given list.'''
	items_queue = LockQueue()
	
	global g_process_jobs

	for n in range(8):
		job = ExtQueueThread(items_queue, func)
		g_process_jobs.append(job)

	# Start processing threads over the object-queue 
	for j in g_process_jobs:
		j.start()

	# Fill file-queue with pathes of files
	for i in items:
		items_queue.put_item_blocked(i)

	# Wait for the queue to be empty
	while items_queue.size() > 0:
		time.sleep(0.05)

	process_terminate()

