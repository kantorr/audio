'''
Simple timer.

Date:
	17 October 2022

Author:
	kantorr
'''

import time

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Timer:

	# -------------------------------------------------------------------------
	def __init__(self, start = False) -> None:
		self._start = None if start is False else self.start()
		self._check_point = None

	# -------------------------------------------------------------------------
	def start(self) -> float:
		self._start = time.time()
		return self._start

	# -------------------------------------------------------------------------
	def check_point(self) -> float:
		self._check_point = time.time()
		return self._check_point

	# -------------------------------------------------------------------------
	def diff(self, checkpoint = None) -> tuple:
		delta = self._check_point - self._start
		if checkpoint:
			delta = checkpoint - self._start

		hours = int(delta / 3600)
		minutes = int((delta - hours * 3600) / 60)
		seconds = round((delta - (hours * 3600)) - (minutes * 60), 4)

		return (hours, minutes, seconds)

