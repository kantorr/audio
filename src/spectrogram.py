from .logger import Log
from .logger import LogStdIOThread
from .file_system import is_exists
from .file_system import is_file
from .file_system import is_directory
from .file_system import mkdir
from .file_system import File as FsFile
from .file_system import Directory as FsDirectory
from .lock_queue import LockQueue
from .queue_thread import ExtQueueThread
from .queue_thread import terminate_threads
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import os
import argparse
import subprocess
import time

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
g_args = None
g_this_script = "spectrogram"
g_output_dir = "spectrograms"

g_log = LogStdIOThread()
g_spectro_jobs = []

# -----------------------------------------------------------------------------
def receive_arguments():
	'''Parsed arguments are accessible through the global variable: g_args.'''
	s = f"{g_this_script}: v.2.0\n"
	s += "Build a spectrogram over all audio files in desired folder or for single audio file.\n"
	s += f'Spectrograms will be builded as PNG files and will be located in: "{g_output_dir}"\n'
	s += "folder inside the required one."
	arg_parser = argparse.ArgumentParser("spectrogram.py", 
		description = s, 
		formatter_class = argparse.RawTextHelpFormatter)
	
	arg_parser.add_argument("target", 
		help = "The desired directory contains audio files or single file.")
	
	arg_parser.add_argument("-v", "--verbose", 
		help = "Verbose output, good for debugging.", 
		action = "store_true")

	global g_args
	g_args = arg_parser.parse_args()

# -----------------------------------------------------------------------------
def file_spectrogram(filename):
	'''Build a spectrogram for single file.'''
	fs_file = FsFile(filename)
	folder = fs_file.parent()
	out_file = os.path.join(folder, g_output_dir, f"{fs_file.name_only()}.png")

	mkdir(os.path.join(folder, g_output_dir))

	g_log.regular("==> " + Log.accent_light("Building spectrogram for: ") + Log.success(fs_file.name_whole()))
	cmd_sox = ['sox', filename, '-n', 'spectrogram', '-o', out_file]
	proc_sox = subprocess.Popen(cmd_sox, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

# -----------------------------------------------------------------------------
def dir_spectrogram(dirname):
	'''Build spectrogram for supported audio files in requested directory.'''

	# Multithreaded processing of collected object-files
	file_queue = LockQueue()
	global g_spectro_jobs

	for i in range(8):
		job = ExtQueueThread(file_queue, file_spectrogram)
		g_spectro_jobs.append(job)

	# Start processing threads over the object-queue 
	for j in g_spectro_jobs:
		j.start()

	fs_dir = FsDirectory(dirname)

	# Fill file-queue with pathes of files
	l_files = [f for f in fs_dir.files(recurcive=False, extensions=[".flac", ".mp3", ".ogg", ".vorbis", ".wav"])]
	g_log.regular("=> " + Log.accent_light(f"Building spectrograms for {len(l_files)} files inside: ") + Log.success(fs_dir.name_whole()))

	for f in l_files:
		file_queue.put_item_blocked(f)

	# Wait for the queue to empty
	while file_queue.size() > 0:
		time.sleep(0.05)

	terminate_threads(g_spectro_jobs)

# -----------------------------------------------------------------------------
def main():
	t_start = time.time()
	g_log.start()

	import traceback

	try:
		receive_arguments()
		global g_args

		if not is_exists(g_args.target):
			raise FileNotFoundError(g_args.target)

		if is_file(g_args.target):
			file_spectrogram(g_args.target)
		elif is_directory(g_args.target):
			dir_spectrogram(g_args.target)

	except KeyboardInterrupt:
		g_log.warning("User interruption!\nGoodbye.")

	except FileNotFoundError as e:
		g_log.error("File not found: " + str(e))

	except SystemExit:
		terminate_threads(g_spectro_jobs)
		g_log.close()
		os._exit(1)

	except Exception as e:
		g_log.error(str(e.__doc__))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		g_log.error(str(e))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_exc()
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_stack()

	finally:
		t_finish = time.time()

		t_delta = t_finish - t_start
		t_hours = int(t_delta / 3600)
		t_minutes = int((t_delta - t_hours * 3600) / 60)
		t_seconds = round((t_delta - (t_hours * 3600)) - (t_minutes * 60), 4)
		g_log.success(f"--- Finished through: {t_hours} hours, {t_minutes} minutes, {t_seconds} seconds")

	terminate_threads(g_spectro_jobs)
	g_log.close()
