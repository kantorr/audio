from .logger import Log
from .logger import LogStdIOThread
from .file_system import is_exists
from .file_system import is_file
from .file_system import is_directory
from .file_system import mkdir
from .file_system import File as FsFile
from .file_system import Directory as FsDirectory
from .lock_queue import LockQueue
from .queue_thread import (process_multiple, process_terminate)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import os
import argparse
import threading
import subprocess
import time

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
g_args = None
g_volume_adjustment = None
g_adjustment_ratio_pool = []

g_log = LogStdIOThread()
g_adjustment_jobs = []
g_ratio_mutex = threading.Lock()

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Config:
	this			= "maximize_volume"
	version			= "2.2"
	dir_out			= "maximized"
	support_formats	= [".flac", ".mp3", ".ogg", ".vorbis", ".wav"]
	description		= "Increase the volume of desired files to maximal without clipping."

# -----------------------------------------------------------------------------
def log_version_heading() -> str:
	g_log.accent_light(f"{Config.this}: " + Log.warning(f"v.{Config.version}"))
	g_log.accent_light(Config.description)
	g_log.accent_light("Result files will be located in: " + Log.warning(f"{Config.dir_out}"))
	g_log.accent_light("folder inside the required one.")

# -----------------------------------------------------------------------------
def build_version_heading() -> str:
	s = f"{Config.this}: v.{Config.version}\n"
	s += f"{Config.description}\n"
	s += f"Result files will be located in: {Config.dir_out}\n"
	s += "folder inside the required one."
	return s

# -----------------------------------------------------------------------------
def receive_arguments():
	'''Parsed arguments are accessible through the global variable: g_args.'''
	arg_parser = argparse.ArgumentParser("maximize_volume.py", 
		description = build_version_heading(), 
		formatter_class = argparse.RawTextHelpFormatter)
	
	arg_parser.add_argument("target", 
		help = "The desired directory contains audio files or single file.")
	
	arg_parser.add_argument("-r", "--ratio", 
		help = "The desired adjustment ratio as floating number, for example: 2.0.")

	arg_parser.add_argument("-v", "--verbose", 
		help = "Verbose output, good for debugging.", 
		action = "store_true")

	global g_args
	g_args = arg_parser.parse_args()

# -----------------------------------------------------------------------------
def get_volume_adjustment_ratio(audiofile: str) -> str:
	'''
	Determine the volume adjustment ration until -1 dB.

	Return:
		The string representing a volume adjustment ratio as floating number.
	'''
	cmd_soxi = ['sox', audiofile, '-n', 'stat', '-v']

	if g_args.verbose is True:
		g_log.regular("==> " + Log.accent_light("Checking: ") + Log.success(audiofile))

	# 	proc_soxi = subprocess.run(cmd_soxi, stdout=subprocess.PIPE, text=True)
	# else:
	proc_soxi = subprocess.run(cmd_soxi, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
	str_ratio = proc_soxi.stderr[:-1]

	if g_args.verbose is True:
		g_log.regular("==> " + Log.accent_light(f"{audiofile}: adjustment: ") + Log.success(str_ratio))
	# 	g_log.regular("==> " + Log.accent_light(f"{audiofile}: stdout: ") + Log.success(proc_soxi.stdout[:-1]))
	# 	g_log.regular("==> " + Log.accent_light(f"{audiofile}: stderr: ") + Log.success(proc_soxi.stderr[:-1]))

	global g_adjustment_ratio_pool
	global g_ratio_mutex

	with g_ratio_mutex:
		g_adjustment_ratio_pool.append(str_ratio)

	return str_ratio

# -----------------------------------------------------------------------------
def get_volume_adjustment_minimal(files: list) -> str:
	global g_ratio_mutex
	adj_ratio_min = None

	process_multiple(files, get_volume_adjustment_ratio)

	global g_adjustment_ratio_pool
	for ratio in g_adjustment_ratio_pool:
		flt_ratio = float(ratio)
		
		g_ratio_mutex.acquire()
		if adj_ratio_min is None or adj_ratio_min > flt_ratio:
			adj_ratio_min = flt_ratio
		g_ratio_mutex.release()

	return str(adj_ratio_min)

# -----------------------------------------------------------------------------
def file_maximize_volume(filename):
	'''Increase the volume to maximal without clipping.'''
	global g_volume_adjustment

	if g_volume_adjustment is None:
		adjustment = get_volume_adjustment_ratio(filename)
	else:
		adjustment = g_volume_adjustment

	fs_file = FsFile(filename)
	folder = fs_file.parent()
	out_file = os.path.join(folder, Config.dir_out, f"{fs_file.name_whole()}")

	# if g_args.verbose is True:
	# 	g_log.regular("==> " + Log.accent_light("Source: ") + Log.success(filename))

	mkdir(os.path.join(folder, Config.dir_out))

	cmd_sox = ['sox', '-v', adjustment, filename, out_file]

	g_log.regular("==> " + Log.accent_light(" Maximizing volume up to: ") + 
		Log.accent_strong(adjustment) + Log.accent_light(" in: ") +  
		Log.success(fs_file.name_whole()))

	if g_args.verbose is True:
		g_log.regular("==> " + Log.accent_light("      Destination: ") + Log.success(out_file))
		g_log.regular("==> " + Log.accent_light("        Operation: ") + " ".join(cmd_sox))

	proc_sox = subprocess.Popen(cmd_sox, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

# -----------------------------------------------------------------------------
def dir_maximize_volume(dirname):
	'''Increase the volume for files in requested directory.'''

	# Gget list of supported files in this directory
	fs_dir = FsDirectory(dirname)
	l_files = [f for f in fs_dir.files(recurcive=False, extensions=Config.support_formats)]

	g_log.regular("=> " + Log.accent_light(f"Increasing volume for {len(l_files)} files inside: ") + Log.success(fs_dir.name_whole()))

	global g_volume_adjustment

	if g_args.ratio:
		g_log.regular("=> " + Log.accent_light(f"The minimal adjustment raion as given: {g_args.ratio}"))
		g_volume_adjustment = g_args.ratio
	else:
		g_log.regular("=> " + Log.accent_light("Calculating the minimal adjustment ratio ..."))
		g_volume_adjustment = get_volume_adjustment_minimal(l_files)

	g_log.regular("=> " + Log.accent_strong("Minimal adjustment ratio: ") + Log.success(g_volume_adjustment))

	# Multithreaded processing of collected object-files
	process_multiple(l_files, file_maximize_volume)

# -----------------------------------------------------------------------------
def main():
	t_start = time.time()
	g_log.start()

	import traceback

	try:
		receive_arguments()
		global g_args

		log_version_heading()

		if not is_exists(g_args.target):
			raise FileNotFoundError(g_args.target)

		if is_file(g_args.target):
			file_maximize_volume(g_args.target)
		elif is_directory(g_args.target):
			dir_maximize_volume(g_args.target)

	except KeyboardInterrupt:
		g_log.warning("User interruption!\nGoodbye.")

	except FileNotFoundError as e:
		g_log.error("File not found: " + str(e))

	except SystemExit:
		process_terminate()
		g_log.close()
		os._exit(1)

	except Exception as e:
		g_log.error(str(e.__doc__))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		g_log.error(str(e))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_exc()
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_stack()

	finally:
		t_finish = time.time()

		t_delta = t_finish - t_start
		t_hours = int(t_delta / 3600)
		t_minutes = int((t_delta - t_hours * 3600) / 60)
		t_seconds = round((t_delta - (t_hours * 3600)) - (t_minutes * 60), 4)
		g_log.success(f"--- Finished through: {t_hours} hours, {t_minutes} minutes, {t_seconds} seconds")

	process_terminate()
	g_log.close()
