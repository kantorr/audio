from .logger import Log
from .logger import LogStdIOThread
from .file_system import is_exists
from .file_system import is_file
from .file_system import is_directory
from .file_system import remove_file
from .file_system import mkdir
from .file_system import File as FsFile
from .file_system import Directory as FsDirectory
from .lock_queue import LockQueue
from .queue_thread import ExtQueueThread
from .queue_thread import terminate_threads

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import os
import platform
import argparse
import subprocess
import time

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
g_args = None

g_log = LogStdIOThread()
g_extract_jobs = []

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Config:
	this			= "extract"
	sox_verbose		= "-S"
	ffmpeg_quite	= ["-v", "quiet"]
	# Audio-filter to gather statistic over an audio-stream
	ffmpeg_stat		= "volumedetect"
	# Audio-filter to down-mix an audio-stream from 5.1 to stereo
	ffmpeg_downmix_6 = "pan=stereo|FL=0.5*FC+0.707*FL+0.707*BL+0.5*LFE|FR=0.5*FC+0.707*FR+0.707*BR+0.5*LFE"
	ffmpeg_downmix_8 = "pan=stereo|FL=0.5*FC+0.707*FL+0.5*SL+0.707*BL+0.5*LFE|FR=0.5*FC+0.707*FR+0.5*SR+0.707*BR+0.5*LFE"
	file_extension_map = {
		'flac' : '.flac',
		'aac' : '.m4a',
		'mp3' :	'.mp3',
		'ogg' : '.ogg',
		'wav' : '.wav'
	}

	# -------------------------------------------------------------------------
	@staticmethod
	def is_cpu_apple_m1() -> bool:
		return platform.system() == "Darwin" and platform.machine() == "arm64"

# -----------------------------------------------------------------------------
def receive_arguments():
	'''Parsed arguments are accessible through the global variable: g_args.'''
	s = f"{Config.this}: v.3.0\n"
	s += "Extract audio tracks from multimedia (video) container."
	arg_parser = argparse.ArgumentParser(f"{Config.this}.py", 
		description=s, 
		formatter_class=argparse.RawTextHelpFormatter)

	arg_parser.add_argument("target", 
		help="The video-file (container) containing the desired audio-track.")

	s =  "The number of desired audio-track:\n"
	s += "       n - Single track (1 or 2)\n"
	s += "sequence - Multiple track extraction (1,2,5,7,16)\n"
	s += "     all - Extraction of all audio tracks (a or all)\n"

	arg_parser.add_argument("-t", "--track", help=s)

	arg_parser.add_argument("-c", "--codec", 
		help="The name of audio codec: (ffmpeg -codecs) [default is: copy]", 
		default='copy')

	arg_parser.add_argument("-b", "--bitrate", 
		help="The desired bitrate: 320k for MP3 or 529k for AAC")

	arg_parser.add_argument("-d", "--dynrange", 
		help="The desired dynamic-range in bits [default is: 16]", choices=['16', '32'], default='16')

	arg_parser.add_argument("-m", "--multi", 
		help="The number of channels for down-mixing the multichannel track to stereo", choices=['6', '8'])

	s =  "The suffix for output file (according to track-number: -t):\n"
	s += "  single - Single suffix (Rus or Eng)\n"
	s += "sequence - Multiple suffixes according to -t (Rus,Fra,Eng)\n"

	arg_parser.add_argument("-s", "--suffix", help=s)

	arg_parser.add_argument("-v", 
		"--verbose", 
		help="Verbose output, good for debugging.", 
		action="store_true")

	global g_args
	g_args = arg_parser.parse_args()

# -----------------------------------------------------------------------------
def build_ffmpeg_cmd(args: tuple) -> list:
	container = args[0]
	track = args[1]
	suffix = args[2]

	fs_container = FsFile(container)

	out_file = os.path.join(os.getcwd(), fs_container.name_only())

	audio_codec = g_args.codec.lower()

	if track is None:
		out_file += '.mkv'
	else:
		out_file += f".{track:02d}"
		
		if suffix is not None:
			out_file += f".{suffix}"

		out_file += Config.file_extension_map[audio_codec]
		
	cmd = ['ffmpeg', '-y', '-i', f'"{container}"']

	if g_args.verbose is False:
		cmd.extend(Config.ffmpeg_quite)

	if track is None:
		track = 'a'

	# Set the audio-encoder to be AAC Apple M1 native GPU encoder
	if audio_codec == 'aac' and Config.is_cpu_apple_m1():
		audio_codec = 'aac_at'

	cmd.extend(['-stats', f'-map 0:{track}', f'-c:a {audio_codec}'])

	if g_args.bitrate:
		cmd.append(f'-b:a {g_args.bitrate}')

	if g_args.dynrange:
		cmd.append(f'-sample_fmt s{g_args.dynrange}')

	if audio_codec == 'copy':
		audio_filter = ""
	elif g_args.multi:
		# Set the downmix audio-filter
		if int(g_args.multi) == 6:
			audio_filter = f'-af "{Config.ffmpeg_downmix_6}"'
		elif int(g_args.multi) == 8:
			audio_filter = f'-af "{Config.ffmpeg_downmix_8}"'
		else:
			raise Exception(f'Ambigous multi-channel number: "{g_args.multi}" type: {type(g_args.multi)}')
	else:
		audio_filter = f'-af {Config.ffmpeg_stat}'

	if len(audio_filter) > 0:
		cmd.append(audio_filter)

	cmd.append(f'"{out_file}"')

	return cmd

# -----------------------------------------------------------------------------
def extract_track(args: tuple):
	'''
	Extract single audio-track from given container.
	
	Arguments:
		args - The tuple of form: (container: str, track: int, suffix: str = None)
	'''
	container = args[0]
	track = args[1]
	suffix = args[2]

	cmd_ffmpeg = build_ffmpeg_cmd(args)

	fs_out_file = FsFile(cmd_ffmpeg[-1])
	g_log.regular("Extracting: " + Log.accent_light(fs_out_file.name_whole()))

	cmd_line = " ".join(cmd_ffmpeg)
	if g_args.verbose:
		g_log.regular("==> " + Log.warning(cmd_line))
	subprocess.run(cmd_line, shell=True)

# -----------------------------------------------------------------------------
def extract_all_tracks(container: str):
	'''
	Extract all audio tracks from given container.
	'''
	cmd = build_ffmpeg_cmd((container, None, None))
	fs_out_file = FsFile(cmd[-1])

	g_log.regular("Extracting ALL audio tracks to: " + Log.accent_light(fs_out_file.name_whole()))

	cmd_line = " ".join(cmd)
	if g_args.verbose:
		g_log.regular("==> " + Log.warning(cmd_line))
	subprocess.run(cmd_line, shell=True)

# -----------------------------------------------------------------------------
def extract_audio(container: str):
	'''Extract audio-tracks by user request from given container.'''

	track_numbers = [int(t.strip()) if t.strip().isnumeric() else t.strip() for t in g_args.track.split(',')]
	suffixes = None
	if g_args.suffix is not None:
		suffixes = [s.strip() for s in g_args.suffix.split(',')]

	if len(track_numbers) > 1:
		# Multithreaded track extraction
		track_queue = LockQueue()
		global g_extract_jobs

		for i in range(8):
			job = ExtQueueThread(track_queue, extract_track)
			g_extract_jobs.append(job)

		# Start processing threads over the track-queue 
		for j in g_extract_jobs:
			j.start()

		# Fill the queue with arguments
		for i in range(len(track_numbers)):
			suffix = None
			if suffixes is not None:
				if len(suffixes) == len(track_numbers):
					suffix = suffixes[i]
				elif len(suffixes) == 1:
					suffix = suffixes[0]
				else:
					g_log.critical("Ambiguous number of suffixes! Not compatible with number of tracks!")
					exit(1)

			track_queue.put_item_blocked((container, track_numbers[i], suffix))

		# Wait for the queue to empty
		while track_queue.size() > 0:
			time.sleep(0.05)

		terminate_threads(g_extract_jobs)

	else:
		if track_numbers[0] in ['a', 'all', 'A', 'ALL']:
			extract_all_tracks(container)
		else:
			extract_track((container, track_numbers[0], g_args.suffix))

# -----------------------------------------------------------------------------
def main():
	t_start = time.time()
	g_log.start()

	import traceback

	try:
		receive_arguments()
		global g_args

		if not is_exists(g_args.target):
			raise FileNotFoundError(g_args.target)

		if is_file(g_args.target):
			extract_audio(g_args.target)
		elif is_directory(g_args.target):
			g_log.warning(f"Target is directory: {g_args.target}")
			g_log.error("Choose a single file, not directory!")

	except KeyboardInterrupt:
		g_log.warning("User interruption!\nGoodbye.")

	except FileNotFoundError as e:
		g_log.error("File not found: " + str(e))

	except SystemExit:
		terminate_threads(g_extract_jobs)
		g_log.close()
		os._exit(1)

	except Exception as e:
		g_log.error(str(e.__doc__))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		g_log.error(str(e))
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_exc()
		print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		traceback.print_stack()

	finally:
		t_finish = time.time()

		t_delta = t_finish - t_start
		t_hours = int(t_delta / 3600)
		t_minutes = int((t_delta - t_hours * 3600) / 60)
		t_seconds = round((t_delta - (t_hours * 3600)) - (t_minutes * 60), 4)
		g_log.success(f"--- Finished through: {t_hours} hours, {t_minutes} minutes, {t_seconds} seconds")

	terminate_threads(g_extract_jobs)
	g_log.close()
	os._exit(0)
