# audio

Several tools for audio manipulation.  
Each tool has usage information that might be issued by running a tool without arguments.  

## `dsf2human`
---

Convert `DSF` (`DSD`) files to high end `FLAC` files through Band-Pass filter of human hearing frequencies range.  

### Usage

```shell
usage: dsf2human.py [-h] [-m {6,8}] [-r {44100,48000}] [-b {0,1,2}] [-v] target

dsf2human: v.2.4
Convert from DSF (DSD) to 24-bit FLAC format in 48 or 44.1 kHz sample-rate.
The Band-Pass filter of human hearing frequencies range is apllyied.
The output directory: FLAC-HUMAN

positional arguments:
  target                The single file to convert or folder contains audio files to be converted.

options:
  -h, --help            show this help message and exit
  -m {6,8}, --multi {6,8}
                        Downmix surround multi-track to stereo.
                        Combining 5.1 multi-track as: 0.5*Center + 0.707*Front + 0.707*Back + 0.5*LFE
                        Combining 7.1 multi-track as: 0.5*Center + 0.707*Front + 0.5*Side + 0.707*Back + 0.5*LFE
  -r {44100,48000}, --rate {44100,48000}
                        Convert to desired rate_value (-r 44100, --rate 48000).
                        The default rate is 48 kHz.
  -b {0,1,2}, --bpf {0,1,2}
                        Apply Band-Pass filter with desired band of frequencies (-b 0, -b 1, --bpf 2).
                        The corresponding bands are: 0: 5Hz - 24kHz (default), 1: 20Hz - 20kHz, 2: 24Hz - 16kHz.
  -v, --verbose         Verbose output, good for debugging.
```

### sacd_extract

The `DSF` files usually produced by extracting `SACD` image by [sacd_extract](https://www.videohelp.com/software/sacd-extract) utility.  

```shell
> sacd_extract -sC -i image.iso
```

## `human_band`
---

Cleanup high end audio files to human hearing frequencies range.  

### Usage

```shell
usage: human_band.py [-h] [-r {44100,48000}] [-b {0,1,2}] [-v] target

human_band: v.2.2
Apply a sinc kaiser-windowed Band-Pass Filter in human hearing frequencies range
on to files in FLAC format with sample-rate based on 48 kHz (48, 96, 192 kHz) or
44.1 kHz (44.1, 88.2, 176.4 kHz).
The output directory: FLAC-HUMAN

positional arguments:
  target                The single file to convert or folder contains audio files to be converted.

options:
  -h, --help            show this help message and exit
  -r {44100,48000}, --rate {44100,48000}
                        Convert to desired sample-rate (-r 44100, --rate 48000).
                        The default rate is 48 kHz.
  -b {0,1,2}, --bpf {0,1,2}
                        Apply Band-Pass filter with desired band of frequencies (-b 0, -b 1, --bpf 2).
                        The corresponding bands are: 0: 5Hz - 24kHz (default), 1: 20Hz - 20kHz, 2: 24Hz - 16kHz.
  -v, --verbose         Verbose output, good for debugging.
```

## `extract`
---

Extract audio tracks from multimedia (video) container.  

### Usage

```shell
usage: extract.py [-h] [-t TRACK] [-c CODEC] [-b BITRATE] [-d {16,32}] [-m {6,8}] [-s SUFFIX] [-v] target

extract: v.2.0
Extract audio tracks from multimedia (video) container.

positional arguments:
  target                The video-file (container) containing the desired audio-track.

options:
  -h, --help            show this help message and exit
  -t TRACK, --track TRACK
                        The number of desired audio-track:
                               n - Single track (1 or 2)
                        sequence - Multiple track extraction (1,2,5,7,16)
                             all - Extraction of all audio tracks (a or all)
  -c CODEC, --codec CODEC
                        The name of audio codec: (ffmpeg -codecs) [default is: copy]
  -b BITRATE, --bitrate BITRATE
                        The desired bitrate: 320k for MP3 or 529k for AAC
  -d {16,32}, --dynrange {16,32}
                        The desired dynamic-range in bits
  -m {6,8}, --multi {6,8}
                        The number of channels for down-mixing the multichannel track to stereo
  -s SUFFIX, --suffix SUFFIX
                        The suffix for output file (according to track-number: -t):
                          single - Single suffix (Rus or Eng)
                        sequence - Multiple suffixes according to -t (Rus,Fra,Eng)
  -v, --verbose         Verbose output, good for debugging.
```

### Extract three 5.1 surround tracks through down-mixing to stereo

Extract three multi-channel 5.1 tracks as a down-mixed stereo tracks.  

```shell
> python extract.py video_container.mkv -t 1,3,5 -s Rus,Ger,Eng -m 6 -c flac -d 32

> ls
video_container.mkv
video_container.01.Rus.flac
video_container.03.Ger.flac
video_container.05.Eng.flac
```
### Extract all audio track as 5.1 surround through down-mixing them to stereo

Extract all audio tracks from the `video_container.mkv` file as down-mixed stereo tracks:  

```shell
python extract.py video_container.mkv -t a -c flac -m 6
```

One file will be created near the `video_container.mkv` file, in the same directory: `video_container-audio-tracks.mkv`.  
All audio tracks will be saved as down-mixed stereo in `FLAC` format.  

### Copy all audio tracks

Copy all audio tracks from the `video_container.mkv` file:

```shell
python extract.py video_container.mkv -t a -c copy
```

One file will be created near the `video_container.mkv` file, in the same directory: `video_container-audio-tracks.mkv`.  
This file will contain exact copies of all audio tracks from the `video_container.mkv` file, without all other tracks, video, subtitles, etc.  

## `normalize-cinema`
---

Normalization of cinematographic audio tracks with following parameters:  
- Normalization algorithm: [EBU R 128](https://en.wikipedia.org/wiki/EBU_R_128)
- Maximal peak level: `+0.0 dB`
- Lossless quality Audio Encoder: [FLAC](https://en.wikipedia.org/wiki/FLAC)
  - Audio Bitrate: as in the original audio track
  - Audio Sample Rate: as in the original audio track
- Loss quality Audio Encoder: [AAC](https://en.wikipedia.org/wiki/Advanced_Audio_Coding)
  - Audio Bitrate: `529k` `VBR`
  - Audio Sample Rate: `48 kHz`

### Usage

```shell
usage: normalize_cinema.py [-h] [-l] target

normalize_cinema: v.1.0
Normalize audio file for comfort cinema experience.
Normalized files will be placed in "NORMALIZED_FOR_CINEMA" folder inside the required one
Normalization will be taken by ffmpeg-normalize utility
The EBU R128 algorithm will be applied with following quality:
  Lossed: Maximal peak level: +0.0 dB, Encoder: AAC, Bitrate: 529k VBR, Sample Rate: 48 kHz
Lossless: Maximal peak level: +0.0 dB, Encoder: FLAC, Bitrate: as sourced, Sample Rate: as sourced

positional arguments:
  target          The desired directory contains audio files or single file.

options:
  -h, --help      show this help message and exit
  -l, --lossless  Lossless output, high quality audio output in FLAC format.
```

## `spectogram`
---

Build a visual spectogram over desired audio-files.

### Usage

```shell
usage: spectrogram.py [-h] [-v] target

spectrogram: v.2.0
Build a spectrogram over all audio files in desired folder or for single audio file.
Spectrograms will be builded as PNG files and will be located in: "spectrograms"
folder inside the required one.

positional arguments:
  target         The desired directory contains audio files or single file.

options:
  -h, --help     show this help message and exit
  -v, --verbose  Verbose output, good for debugging.
```

## `maximize_volume`
---

Increase the volume of desired files to maximal without clipping.  
In case of group of files (directory) the minimal adjustment ratio will be determined and applied on every file of a group.  

### Usage

```shell
usage: maximize_volume.py [-h] [-r RATIO] [-v] target

maximize_volume: v.2.2
Increase the volume of desired files to maximal without clipping.
Result files will be located in: maximized
folder inside the required one.

positional arguments:
  target                The desired directory contains audio files or single file.

options:
  -h, --help            show this help message and exit
  -r RATIO, --ratio RATIO
                        The desired adjustment ratio as floating number, for example: 2.0.
  -v, --verbose         Verbose output, good for debugging.
```

## Dependencies
---

### [Python 3.x](https://www.python.org)

Install `Python 3.x` version:  

```bash
> sudo apt install python3
```

### [ffmpeg](https://ffmpeg.org)

Install ffmpeg from your distribution.  

Linux:

```shell
> sudo apt install ffmpeg
```

Mac OS X:  

```shell
> brew install ffmpeg
```

The latest version might be installed from http://ffmpeg.org/, it should be added in the `$PATH` environment.  

### [SoX](http://sox.sourceforge.net)

[SoX](http://sox.sourceforge.net) installation is required.  
On install Sox by from your distribution.  

```shell
> sudo apt install sox
```

### [ffmpeg-normalize](https://github.com/slhck/ffmpeg-normalize)

[ffmpeg-normalize](https://pypi.org/project/ffmpeg-normalize/) installation required as well.  
Install it on `Linux` or `mac OS X` by following command:  

```shell
> pip install ffmpeg-normalize
```

Consider to use virtual environments to avoid mess with different python installation on your machine.   

```shell
> pip install virtualenv

# Python 3
> python3 -m venv audio-normalize
> source audio-normalize/bin/activate
(audio-normalize) > pip install ffmpeg-normalize
(audio-normalize) > deactivate
>
```

---

## License

The [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html) (GPLv3)

Audio manipulation tools.  
Copyright (C) 2020  Ruslan Kantorovych  

This set of programs is free software: you can redistribute it   
and/or modify it under the terms of the GNU General Public License   
as published by the Free Software Foundation, version 3 of the  
License (GPLv3).  
  
This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU General Public License for more details.  
  
You should have received a copy of the GNU General Public License  
along with this program.  If not, see https://www.gnu.org/licenses/.  

